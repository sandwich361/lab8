"""lab8_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from project8.views import Home, LoginView, RegisterView, Users, Tables, Game


urlpatterns = [
    path('admin/', admin.site.urls),
    path('admin/dashboard', Home.as_view()),
    path('login/', LoginView.as_view(), name = "login"),
    path('register/', RegisterView.as_view(), name = "register"),
    path('tables/', Tables.as_view(), name = "alltables"),
    path('users/', Users.as_view(), name = "users"),
    path('game/<str:tablename>/', Game.as_view(), name = "table"),
]
