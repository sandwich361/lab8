import unittest
from project8.Classes import Card, Dealer, Deck, Player
from project8.models import CardModel, DealerModel, DeckModel, PlayerModel


class TestDealerInit(unittest.TestCase):
    def setUp(self):
        self.maindealer = Dealer("maindealer")
        self.gooddealer = Dealer("gooddealer")
        self.deck = Deck()
        self.baddeck = [Card(1, 2), Card(3, 4), Card(2, 13)]



    def test_dealer_init_good(self):
        self.assertEqual(True, self.maindealer.isonline)
        self.assertEqual(self.deck, self.maindealer.deck)
        self.assertEqual(52, self.maindealer.deck.count())

        self.assertEqual(True, self.gooddealer.isonline)
        self.assertEqual(self.deck, self.gooddealer.deck)
        self.assertEqual(52, self.gooddealer.deck.count())

        # make sure dealer is online after method
        self.assertNotEqual(False, self.maindealer.isonline)
        self.assertNotEqual(False, self.gooddealer.isonline)

        self.assertNotEqual(self.baddeck, self.gooddealer.deck)
        self.assertNotEqual(self.baddeck, self.maindealer.deck)

    def test_dealer_init_no_parameter(self):
        with self.assertRaises(NameError) as context1:
            self.dealer1 = Dealer()
        self.assertEqual('No inputs provided', str(context1.exception))

    def test_dealer_init_wrong_parameter(self):
        with self.assertRaises(TypeError) as context1:
            self.dealer1 = Dealer(123)
        self.assertEqual('Incorrect data type provided for username', str(context1.exception))

        with self.assertRaises(TypeError) as context1:
            self.dealer1 = Dealer(['a', 'b', 'c'])
        self.assertEqual('Incorrect data type provided for username', str(context1.exception))

        with self.assertRaises(TypeError) as context1:
            self.dealer1 = Dealer(True)
        self.assertEqual('Incorrect data type provided for username', str(context1.exception))


