from django.test import TestCase
from project8.models import PlayerModel
from project8.models import PokerTableModel


class TestPlayerSeeChips(TestCase):

    # setup for testing. will create testPlayer and testTable in the DB
    def setUp(self):
        username = "testPlayer1"
        tablename = "testTable1"
        dealername = "testDealer"

        if (PlayerModel.objects.filter(username__exact=username)):
            player = PlayerModel.objects.get(username__exact=username)
        else:
            PlayerModel.objects.create(username=username, isonline=True)
            player = PlayerModel.objects.get(username__exact=username)

        if (PokerTableModel.objects.filter(tablename__exact=tablename)):
            table = PokerTableModel.objects.get(tablename__exact=tablename)
        else:
            PokerTableModel.objects.create(tablename=tablename, dealer=dealername)
            table = PokerTableModel.objects.get(tablename__exact=tablename)

        player.chips = 100
        player.isonline = True
        table.currentBet = 0
        table.pot = 0
        table.players = player
        player.save()
        table.save()

    # ensure bad usernames don't return successfully
    def test_seechips_badUserName(self, player, table):
        self.assertEquals(player.seechips(self, player, table), "player has 100 chips")
        self.assertNotEquals(player.seechips(self, "playerz", table), "player has 100 chips")
        self.assertEquals(player.seechips(self, "playerz", table), "Error: playerz does not exist")

    # ensure bad tablenames don't return successfully
    def test_seechips_badTableName(self, player, table):
        self.assertEquals(player.seechips(self, player, table), "player has 100 chips")
        self.assertNotEquals(player.seechips(self, player, "tablez"), "player has 100 chips")
        self.assertEquals(player.seechips(self, player, "tablez"), "Error: tablez does not exist")

    # ensure player chips are updated correctly
    def test_seechips_playerChips(self, player, table):
        player.chips = 100
        player.save()
        self.assertEquals(player.seechips(self, player, table), "player has 100 chips")
        player.chips = 90
        player.save()
        self.assertEquals(player.seechips(self, player, table), "player has 90 chips")
        self.assertNotEquals(player.seechips(self, player, table), "player has 100 chips")