import unittest
from project8.Classes.Card import Card

class TestGetValue(unittest.TestCase):
    def setUp(self):
        self.card1 = Card(1, 8)
        self.card2 = Card(2, 3)
        self.card3 = self.card1
        self.card4 = Card(3, 10)
        self.card5 = Card(4, 13)
        self.card6 = self.card4

    # Author: Nicholas
    def test_get_value(self):
        print(self.card1.getValue())
        self.assertEqual(self.card1.getValue(), self.card3.getValue())
        self.assertNotEqual(self.card1.getValue(), self.card2.getValue())
        self.assertNotEqual(self.card4.getValue(), self.card2.getValue())
        self.assertNotEqual(self.card5.getValue(), self.card4.getValue())
        self.assertEqual(self.card6.getValue(), self.card4.getValue())


    def test_get_value_bad_suit(self):
        with self.assertRaises(ValueError) as context1:
            self.card1.s = 0
        self.assertEqual(str(context1), 'cannot get value of card', self.card1.getValue())
        with self.assertRaises(ValueError) as context1:
            self.card2.s = -1
        self.assertEqual(str(context1), 'cannot get value of card', self.card2.getValue())

    def test_get_value_bad_rank(self):
        with self.assertRaises(ValueError) as context1:
            self.card4.r = 0
        self.assertEqual(str(context1), 'cannot get value of card', self.card4.getValue())
        with self.assertRaises(ValueError) as context1:
            self.card5.r = -1
        self.assertEqual(str(context1), 'cannot get value of card', self.card5.getValue())





if __name__ == '__main__':
    unittest.main()
