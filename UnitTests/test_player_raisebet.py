from django.test import TestCase
from project8.models import PlayerModel
from project8.models import PokerTableModel

class TestPlayerRaiseBet(TestCase):

    # setup for testing. will create testPlayer and testTable in the DB
    def setUp(self):
        username = "testPlayer1"
        tablename = "testTable1"
        dealername = "testDealer"

        if (PlayerModel.objects.filter(username__exact=username)):
            player = PlayerModel.objects.get(username__exact=username)
        else:
            PlayerModel.objects.create(username=username, isonline=True)
            player = PlayerModel.objects.get(username__exact=username)

        if (PokerTableModel.objects.filter(tablename__exact=tablename)):
            table = PokerTableModel.objects.get(tablename__exact=tablename)
        else:
            PokerTableModel.objects.create(tablename=tablename, dealer=dealername)
            table = PokerTableModel.objects.get(tablename__exact=tablename)

        player.chips = 100
        player.isonline = True
        table.currentBet = 0
        table.pot = 0
        table.players = player
        player.save()
        table.save()

    # ensure outerbounds for bet hold
    def test_raisebet_outerBounds(self, player, table):
        self.assertEquals(player.raisebet(self, 5, player, table), "player raised bet to 5")
        self.assertNotEquals(player.raisebet(self, 105, player, table), "player raised bet to 105")
        self.assertEquals(player.raisebet(self, 105, player, table), "Error: not enough chips")
        self.assertNotEquals(player.raisebet(self, -5, player, table), "player raised bet to -5")

    # ensure bad value inputs for chips are caught
    def test_raisebet_badChipsInput(self, player, table):
        table.currentBet = 0
        table.save()
        self.assertEquals(player.raisebet(self, 5, player, table), "player raised bet to 5")
        self.assertNotEquals(player.raisebet(self, "5", player, table), "player raised bet to 5")
        self.assertNotEquals(player.raisebet(self, "abc", player, table), "player raised bet to abc")

    # ensure bad usernames don't return successfully
    def test_raisebet_badUserName(self, player, table):
        table.currentBet = 0
        table.save()
        self.assertEquals(player.raisebet(self, 5, player, table), "player raised bet to 5")
        self.assertNotEquals(player.raisebet(self, 5, "playerz", table), "playerz raised bet to 5")
        self.assertEquals(player.raisebet(self, 5, "playerz", table), "Error: playerz does not exist")

    # ensure bad tablenames don't return successfully
    def test_raisebet_badTableName(self, player, table):
        table.currentBet = 0
        table.save()
        self.assertEquals(player.raisebet(self, 5, player, table), "player raised bet to 5")
        self.assertNotEquals(player.raisebet(self, 5, player, "tablez"), "player raised bet to 5")
        self.assertEquals(player.bet(self, 5, player, "tablez"), "Error: tablez does not exist")

    # ensure table pot field updated correctly
    def test_raisebet_tablePot(self, player, table):
        table.pot = 0
        player.chips = 100
        table.save()
        player.save()
        self.assertEquals(table.pot, 0)
        player.raisebet(self, 5, player, table)
        self.assertEquals(table.pot, 5)
        self.assertNotEquals(table.pot, 0)

    # ensure table currentBet field updated correctly
    def test_raisebet_tableCurrentBet(self, player, table):
        table.currentBet = 5
        player.chips = 100
        table.save()
        player.save()
        self.assertEquals(table.currentBet, 5)
        player.raisebet(self, 10, player, table)
        self.assertEquals(table.currentBet, 15)
        self.assertNotEquals(table.currentBet, 5)
        self.assertNotEquals(table.currentBet, 10)
        player.raisebet(self, 5, player, table)
        self.assertEquals(table.currentBet, 20)
        self.assertNotEquals(table.currentBet, 10)
        self.assertNotEquals(table.currentBet, 15)
        self.assertNotEquals(table.currentBet, 5)

    # ensure player chips are updated correctly
    def test_raisebet_playerChips(self, player, table):
        player.chips = 100
        table.currentBet = 5
        player.save()
        table.save()
        self.assertEquals(player.chips, 100)
        player.raisebet(self, 5, player, table)
        self.assertEquals(player.chips, 95)
        player.raisebet(self, 15, player, table)
        self.assertEquals(player.chips, 80)
        self.assertEquals(player.raisebet(self, 85, player, table), "Error: bad bet chips value")
        self.assertNotEquals(player.raisebet(self, 85, player, table), "player successfully bet 85 chips")
