from django.test import TestCase
from project8.models import PlayerModel
from project8.models import PokerTableModel


class TestPlayerCall(TestCase):

    # setup for testing. will create testPlayer and testTable in the DB
    def setUp(self):
        username = "testPlayer1"
        tablename = "testTable1"
        dealername = "testDealer"

        if (PlayerModel.objects.filter(username__exact=username)):
            player = PlayerModel.objects.get(username__exact=username)
        else:
            PlayerModel.objects.create(username=username, isonline=True)
            player = PlayerModel.objects.get(username__exact=username)

        if (PokerTableModel.objects.filter(tablename__exact=tablename)):
            table = PokerTableModel.objects.get(tablename__exact=tablename)
        else:
            PokerTableModel.objects.create(tablename=tablename, dealer=dealername)
            table = PokerTableModel.objects.get(tablename__exact=tablename)

        player.chips = 100
        player.isonline = True
        table.currentBet = 0
        table.pot = 0
        table.players = player
        player.save()
        table.save()

    # ensure bad usernames don't return successfully
    def test_call_badUserName(self, player, table):
        self.assertEquals(player.call(self, player, table), "player called")
        self.assertNotEquals(player.call(self, "playerz", table), "player called")
        self.assertEquals(player.call(self, "playerz", table), "Error: playerz does not exist")

    # ensure bad tablenames don't return successfully
    def test_call_badTableName(self, player, table):
        self.assertEquals(player.call(self, player, table), "player called")
        self.assertNotEquals(player.call(self, player, "tablez"), "player called")
        self.assertEquals(player.call(self, player, "tablez"), "Error: tablez does not exist")

    # ensure table pot field updated correctly
    def test_call_tablePot(self, player, table):
        table.pot = 5
        table.currentBet = 5
        player.chips = 100
        table.save()
        player.save()
        self.assertEquals(table.pot, 5)
        player.call(self, player, table)
        self.assertEquals(table.pot, 10)
        self.assertNotEquals(table.pot, 5)

    # ensure table currentBet field updated correctly
    def test_call_tableCurrentBet(self, player, table):
        table.currentBet = 5
        table.pot = 5
        player.chips = 100
        table.save()
        player.save()
        self.assertEquals(table.currentBet, 5)
        player.call(self, player, table)
        self.assertEquals(table.currentBet, 5)
        self.assertNotEquals(table.currentBet, 10)

    # ensure player chips are updated correctly
    def test_call_playerChips(self, player, table):
        player.chips = 100
        table.currentBet = 5
        table.pot = 5
        player.save()
        table.save()
        self.assertEquals(player.chips, 100)
        player.call(self, player, table)
        self.assertEquals(player.chips, 95)
        self.assertNotEquals(player.chips, 100)

