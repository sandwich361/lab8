import unittest
from project8.Classes.Card import Card

class TestGetSuite(unittest.TestCase):
    def setUp(self):
        self.card1 = Card(1, 8)
        self.card2 = Card(2, 3)
        self.card3 = self.card1
        self.card4 = Card(3, 10)
        self.card5 = Card(4, 13)
        self.card6 = self.card4


    # Author: Nicholas
    def test_get_suite(self):
        self.assertEqual(1, self.card1.getSuit())
        self.assertNotEqual(self.card1.getSuit(), self.card2.getSuit())
        self.assertEqual(self.card1.getSuit(), self.card3.getSuit())
        self.assertNotEqual(self.card4.getSuit(), self.card5.getSuit())
        self.assertEqual(self.card4.getSuit(), self.card6.getSuit())

    # Author: Aina 4/2
    def test_get_suit_bad(self):
        with self.assertRaises(ValueError) as context1:
            self.card1 = Card(0, 1)
        self.assertEqual(str(context1.exception), "suit value must be between 1 and 4", self.card1.getSuit())
        with self.assertRaises(ValueError) as context1:
            self.card2 = Card(-7, 1)
        self.assertEqual(str(context1.exception), "suit value must be between 1 and 4", self.card2.getSuit())

    # Author: Nicholas
    def test_change_suit(self):
        self.card3.suit = 3
        # rank doesn't change
        self.assertEqual(8, self.card3.getRank())
        self.assertEqual(3, self.card3.getSuit())

    def test_change_suit_bad(self):
        with self.assertRaises(ValueError) as context1:
            self.card3.suit = 0
            self.card3.getSuit()
        self.assertEqual(str(context1.exception), 'cannot get suit of card')





if __name__ == '__main__':
    unittest.main()
