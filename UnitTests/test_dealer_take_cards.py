import unittest
from project8.Classes import Card, Dealer, Deck, Player
from project8.models import CardModel, DealerModel, DeckModel, PlayerModel


class TestDealerTakeCards(unittest.TestCase):
    def setUp(self):
        self.dealer1 = Dealer("dealer1")
        self.dealer2 = Dealer("dealer2")

        self.goodList = [Player("pl1"), Player("pl2"), Player("pl3"), Player("pl4")]
        self.goodList2 = [Player("playerr"), Player("player")]

        self.badList = [Player("player1"), Player("player2"), Card(1, 2)]
        self.badList2 = ["abc", Player("player3")]
        self.badList3 = [True, True, Player(4, 10)]
        self.badList4 = []
        self.badList5 = [Player("p1"), Player("p2"), Player("p3"), Player("p4"), Player("p5"), Player("p6")]

    def test_dealer_take_cards_good(self):
        # make sure deck is back to having 52 cards
        self.dealer1.take_cards(self.goodList)
        self.assertEqual(52, self.dealer1.deck.count())
        # make sure each player now has 0 cards
        for i in range(5):
            self.currentHand = self.goodList[i].hand
            self.assertEqual(0, len(self.currentHand))

        # make sure deck is back to having 52 cards
        self.dealer2.take_cards(self.goodList2)
        self.assertEqual(52, self.dealer2.deck.count())
        # make sure each player now has 0 cards
        for i in range(2):
            self.currentHand1 = self.goodList2[i].hand
            self.assertEqual(0, len(self.currentHand1))

    # test wrong parameters types entered in method
    def test_dealer_take_cards_parameter_type(self):
        with self.assertRaises(TypeError) as context1:
            self.dealer1.take_cards("abc")
        self.assertEqual('Argument is not of type list', str(context1.exception))

        with self.assertRaises(TypeError) as context1:
            self.dealer1.take_cards(123)
        self.assertEqual('Argument is not of type list', str(context1.exception))

        with self.assertRaises(TypeError) as context1:
            self.dealer1.take_cards(True)
        self.assertEqual('Argument is not of type list', str(context1.exception))

        with self.assertRaises(TypeError) as context1:
            self.dealer1.take_cards(Player("testPlayer"))
        self.assertEqual('Argument is not of type list', str(context1.exception))

    # make sure parameter is list of 1-5 players
    def test_dealer_take_cards_parameter_length(self):
        with self.assertRaises(ValueError) as context1:
            self.dealer1.take_cards(self.badList4)
        self.assertEqual('Must provide a list of 1-5 players', str(context1.exception))

        with self.assertRaises(ValueError) as context1:
            self.dealer1.take_cards(self.badList5)
        self.assertEqual('Must provide a list of 1-5 players', str(context1.exception))

    # test what happens when no parameter given
    def test_dealer_take_cards_no_parameter(self):
        with self.assertRaises(NameError) as context1:
            self.dealer1.take_cards()
        self.assertEqual('Must provide a list of players to deal to', str(context1.exception))

    # test to make sure each item in list is a player object
    def test_dealer_take_cards_list_items_type(self):
        with self.assertRaises(TypeError) as context1:
            self.dealer1.take_cards(self.badList)
        self.assertEqual('item in table_players not a Player object', str(context1.exception))

        with self.assertRaises(TypeError) as context1:
            self.dealer1.take_cards(self.badList2)
        self.assertEqual('item in table_players not a Player object', str(context1.exception))

        with self.assertRaises(TypeError) as context1:
            self.dealer1.take_cards(self.badList3)
        self.assertEqual('item in table_players not a Player object', str(context1.exception))
