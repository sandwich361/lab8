import unittest
from project8.Classes.Card import Card

class TestGetRank(unittest.TestCase):
    def setUp(self):
        self.card1 = Card(1, 8)
        self.card2 = Card(2, 13)
        self.card3 = self.card1
        self.card4 = self.card2

    # Author: Aina, Nicholas
    # testing ranks after init
    def test_get_rank(self):
        self.assertEqual(8, self.card1.getRank())
        self.assertNotEqual(13, self.card1.getRank())
        self.assertNotEqual(self.card2.getRank(), self.card1.getRank())
        self.assertEqual(self.card3.getRank(), self.card1.getRank())
        self.assertEqual(self.card2.getRank(), self.card4.getRank())
        self.assertNotEqual(self.card1.getRank(), self.card4.getRank())

    def test_get_rank_bad(self):
        with self.assertRaises(ValueError) as context1:
            self.card1 = Card(1, 0)
        self.assertEqual(str(context1.exception), 'rank value must be between 1 and 14', self.card1.getRank())
        with self.assertRaises(ValueError) as context1:
            self.card2 = Card(1, -7)
        self.assertEqual(str(context1.exception), 'rank value must be between 1 and 14',self.card2.getRank())

    # Author: Aina
    # testing ranks after changes
    def test_change_rank_good(self):
        self.card3.rank = 5
        self.assertEqual(5, self.card3.getRank())

    def test_change_rank_bad(self):
        with self.assertRaises(ValueError) as context1:
            self.card3.rank = 0
            self.card3.getRank()
        self.assertEqual(str(context1.exception), 'cannot get rank of card')

    def test_change_suit(self):
        # changing the suit doesn't change the value returned by get rank
        self.card3.suit = 3
        self.assertEqual(8, self.card3.getRank())



if __name__ == '__main__':
    unittest.main()
