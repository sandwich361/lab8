from django.test import TestCase
from project8.models import PlayerModel, DealerModel
from project8.models import PokerTableModel
from project8.Classes.PokerTable import PokerTable


class TestPlayerBet(TestCase):

    # setup for testing. will create 2 testPlayers and 1 testTable in the DB
    def setUp(self):
        player1name = "testPlayer1"
        player2name = "testPlayer2"
        tablename = "testTable1"
        dealername = "testDealer"
        PlayerModel.objects.create(username=player1name, isonline=True)
        PlayerModel.objects.create(username=player2name, isonline=True)
        self.dealer = DealerModel.objects.create(username=dealername)

        self.table = PokerTableModel.objects.create(name=tablename, dealer=self.dealer)

        self.table.currentBet = 0
        self.table.pot = 0
        self.table.save()

    # test if 1 player at table
    #1 player = NO GAME
    def test_1_players_sitting(self):
        player1 = PlayerModel.objects.get(username__exact="testPlayer1")
        table1 = PokerTableModel.objects.get(name="testTable1")
        table1.players.add(player1)
        table1.save()
        self.assertEqual(table1.players.count(), 1)


        self.assertEqual(PokerTable.game_initialize(PokerTable, tablename=table1.name), "not enough players")
    # test if 2 players are at table
    # 2 players = game starts
    def test_2_players_sitting(self):
        player1 = PlayerModel.objects.get(username__exact="testPlayer1")
        player2 = PlayerModel.objects.get(username__exact="testPlayer2")
        table1 = PokerTableModel.objects.get(name__exact="testTable1")
        table1.players.add(player1,player2)
        table1.save()
        self.assertEqual(self.table.players.count(), 2)
        self.assertEqual(PokerTable.game_initialize(PokerTable, tablename=table1.name), "table created "+
                        "table: testTable1" +
                         "\ndealer: testDealer" +
                         "\nplayers:"
                         "\ntestPlayer1"
                         "\ntestPlayer2" +
                         "\nfolded players:\n")






