import unittest
from project8.Classes.PokerTable import PokerTable
from project8.models import PokerTableModel


class MyTestCase(unittest.TestCase):
    def setUp(self):
        #set up
        pass


    def test_game_driver(self):
        table = PokerTableModel.objects.get(name=self.name)
        game = PokerTable.game_driver(self)
        highplayer = None
        for player in table.players:
            if highplayer == None:
                highplayer = player
            elif player.chips >= highplayer.chips:
                highplayer = player

        if table.players.count() < 2:
            self.assertEqual(PokerTable.game_driver(self), "can not start game, not enough players")
        else:
            self.assertEqual(PokerTable.game_driver(self), "the winner is " + highplayer)




