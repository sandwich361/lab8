import unittest
from project8.Classes.Deck import deck


#2/26 Aina: testing empty deck
class TestDeckDrawException(unittest.TestCase):
    def setUp(self):
        self.deck1 = deck()

    def test_empty_deck(self):
        #draw until no more cards
        for a in range(0, 52):
            self.deck1.draw()

        with self.assertRaises(ValueError) as context1:
            self.deck1.draw()
        self.assertEqual('no more cards', str(context1.exception))


if __name__ == '__main__':
    unittest.main()
