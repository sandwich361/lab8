from django.test import TestCase
from project8.models import PlayerModel
from project8.models import PokerTableModel, AdminModel, DealerModel


class TestPlayerQuit(TestCase):

    # setup for testing. will create testPlayer and testTable in the DB
    from django.test import TestCase
    from project8.models import PlayerModel
    from project8.models import PokerTableModel, AdminModel, DealerModel

    class TestPlayerQuit(TestCase):

        # setup for testing. will create testPlayer and testTable in the DB
        def setUp(self):
            AdminModel.objects.create(username="AdminSandwich", isonline=True)

            # mutliple tables
            dealer = DealerModel.objects.create(username="MasterSandwich", isonline=True)
            PokerTableModel.objects.create(name="table1", dealer=dealer)
            megadealer = DealerModel.objects.create(username="MegaSandwich", isonline=True)
            PokerTableModel.objects.create(name="table2", dealer=megadealer)

            # players
            self.player1 = PlayerModel.objects.create(username="player1", isonline=True)
            self.player2 = PlayerModel.objects.create(username="player2", isonline=True)
            self.player3 = PlayerModel.objects.create(username="player3", isonline=True)
            self.player4 = PlayerModel.objects.create(username="player4", isonline=True)
            self.player5 = PlayerModel.objects.create(username="player5", isonline=True)

            self.table1.players = [self.player1, self.player2, self.player3]


        def test_player_quit_good(self):
            self.table1.pot = 20
            self.player1.chips = 5
            self.player2.chips = 20;
            self.player1.save()
            self.player2.save()
            self.table1.save()
            self.player1.quit_game("table1")
            self.table1.save()
            self.player1.save()
            self.assertEqual(2, len(self.table1.players))
            self.assertEqual(25, self.table1.pot)
            self.assertEqual(True, self.player1.folded)
            self.player2.quit_game("table2")
            self.table2.save()
            self.player2.save()
            self.assertEqual(1, len(self.table1.players))
            self.assertEqual(45, self.table1.pot)
            self.assertEqual(True, self.player1.folded)

        def test_wrong_table_name(self):
            self.assertEqual(self.player1.quit_game("tablee1"), "cannot quit because table1 does not exist")



