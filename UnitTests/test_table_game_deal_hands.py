from django.test import TestCase
from project8.models import PlayerModel, DealerModel, DeckModel
from project8.models import PokerTableModel
from project8.Classes.PokerTable import PokerTable
from project8.Classes.Player import Player
from project8.Classes.Deck import deck
from project8.Classes.Card import Card


class TestTableDealHands(TestCase):

    # setup for testing. will create 2 testPlayers and 1 testTable in the DB
    def setUp(self):
        self.player1 = Player("p1")
        self.player1.hand = [Card(1, 2), Card(3,10), Card(4,6), Card(2, 11), Card(3, 13)]
        self.player2 = Player("p2")
        self.player2.hand = [Card(1, 9), Card(3, 6), Card(4, 7), Card(2, 8), Card(4, 5)]
        self.table_players = [self.player1, self.player2]
        self.dealer = DealerModel.objects.create(username="dealer1")
        self.table = PokerTableModel.objects.create(name="table1", dealer=self.dealer, players=self.table_players)
        self.table.currentBet = 0
        self.table.pot = 0
        self.table.save()

    def test_table_deal_good(self):
        self.oldHand1 = self.player1.hand
        self.oldHand2 = self.player2.hand
        self.table1.activePlayers = self.table_players
        self.table1.dealHands()
        #make sure hand isnt the same as it was
        self.assertNotEqual(self.oldHand1, self.player1.hand)
        self.assertNotEqual(self.oldHand2, self.player2.hand)

    # make sure there is more than one active player to deal hand to
    def test_table_deal_not_enough_active_players(self):
        # try to deal hands with only one player
        self.table_players.clear()
        self.table_players.append(self.player1)
        self.table1.activePlayers = self.table_players
        self.table1.save()

        with self.assertRaises(ValueError) as context1:
            self.table1.dealHands()
        self.assertEqual('not enough active players to deal hand to', str(context1.exception))
        # try to deal with 0 players
        self.table_players.clear()
        self.table1.activePlayers = self.table_players
        self.table1.save()
        with self.assertRaises(ValueError) as context1:
            self.table1.dealHands()
        self.assertEqual('not enough active players to deal hand to', str(context1.exception))
