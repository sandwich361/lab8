import unittest
from project8.Classes.Card import Card

# By: Brad-- testing card toString method

class TestCardStr(unittest.TestCase):
    # create some cards for tests
    def setUp(self):
        self.cardCharacters = Card('a', 'a')
        self.cardLowerBound = Card(0, 0)
        self.cardUpperBound = Card(5, 15)
        self.cardClubs = Card(1, 1)
        self.cardDiamonds = Card(1, 1)
        self.cardHearts = Card(1, 1)
        self.cardSpades = Card(1, 1)

    # test error thrown when card is outside bounds
    #2/27 Aina changed _str_ to str method
    def test_str_invalid(self):
        self.assertEqual(self.cardCharacters.__str__(self), 'INVALID CARD')
        self.assertEqual(self.cardLowerBound.__str__(self), 'INVALID CARD')
        self.assertEqual(self.cardUpperBound.__str__(self), 'INVALID CARD')
        self.assertEqual(self.cardCharacters.__str__(self), 'INVALID CARD')

    # test suits are displayed correctly
    def test_str_suits(self):
        self.assertEqual(self.cardClubs.__str__(self), 'AC')
        self.assertNotEqual(self.cardClubs.__str__(self), 'AD')
        self.assertEqual(self.cardDiamonds.__str__(self), 'AD')
        self.assertNotEqual(self.cardDiamonds.__str__(self), 'AH')
        self.assertEqual(self.cardHearts.__str__(self), 'AH')
        self.assertNotEqual(self.cardHearts.__str__(self), 'AS')
        self.assertEqual(self.cardSpades.__str__(self), 'AS')
        self.assertNotEqual(self.cardSpades.__str__(self), 'AC')

    # test ranks are displayed correctly
    def test_str_ranks(self):
        for i in range(14):
            tmp = Card(1, i)
            if i == 1:
                self.assertEqual(self.tmp.__str__(self), 'AC')
            else:
                self.assertEqual(self.tmp.__str__(self), i + 'C')


if __name__ == '__main__':
    unittest.main()