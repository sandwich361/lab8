from django.test import TestCase
from project8.models import PlayerModel
from project8.models import PokerTableModel


class TestPlayerAllIn(TestCase):

    # setup for testing. will create testPlayer and testTable in the DB
    def setUp(self):
        username = "testPlayer1"
        tablename = "testTable1"
        dealername = "testDealer"

        if (PlayerModel.objects.filter(username__exact=username)):
            player = PlayerModel.objects.get(username__exact=username)
        else:
            PlayerModel.objects.create(username=username, isonline=True)
            player = PlayerModel.objects.get(username__exact=username)

        if (PokerTableModel.objects.filter(tablename__exact=tablename)):
            table = PokerTableModel.objects.get(tablename__exact=tablename)
        else:
            PokerTableModel.objects.create(tablename=tablename, dealer=dealername)
            table = PokerTableModel.objects.get(tablename__exact=tablename)

        player.chips = 100
        player.isonline = True
        table.currentBet = 0
        table.pot = 0
        table.players = player
        player.save()
        table.save()

    # ensure bad usernames don't return successfully
    def test_allin_badUserName(self, player, table):
        player.chips = 100
        table.pot = 100
        table.currentBet = 0
        player.save()
        table.save()
        self.assertEquals(player.allin(self, player, table), "player went all in")
        self.assertNotEquals(player.allin(self, "playerz", table), "player went all in")
        self.assertEquals(player.allin(self, "playerz", table), "Error: playerz does not exist")

    # ensure bad tablenames don't return successfully
    def test_allin_badTableName(self, player, table):
        player.chips = 100
        table.pot = 100
        table.currentBet = 0
        player.save()
        table.save()
        self.assertEquals(player.allin(self, player, table), "player went all in")
        self.assertNotEquals(player.allin(self, player, "tablez"), "player went all in")
        self.assertEquals(player.allin(self, player, "tablez"), "Error: tablez does not exist")

# ensure table pot field updated correctly
    def test_allin_tablePot(self, player, table):
        player.chips = 100
        table.pot = 100
        table.currentBet = 0
        player.save()
        table.save()
        self.assertEquals(table.pot, 0)
        player.allin(self, player, table)
        self.assertEquals(table.pot, 100)
        self.assertNotEquals(table.pot, 0)

    # ensure table currentBet field updated correctly
    def test_allin_tableCurrentBet(self, player, table):
        player.chips = 100
        table.pot = 100
        table.currentBet = 0
        player.save()
        table.save()
        self.assertEquals(table.currentBet, 0)
        player.allin(self, player, table)
        self.assertEquals(table.currentBet, 100)
        self.assertNotEquals(table.currentBet, 0)

    # ensure player chips are updated correctly
    def test_allin_playerChips(self, player, table):
        player.chips = 100
        table.pot = 100
        table.currentBet = 0
        player.save()
        table.save()
        self.assertEquals(player.chips, 100)
        player.allin(self, player, table)
        self.assertEquals(player.chips, 0)
        self.assertNotEquals(player.chips, 100)
