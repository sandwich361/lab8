import unittest
from project8.Classes.Card import Card



#Author: Aina
class CardInitCorrectRankAndSuit(unittest.TestCase):
    def setUp(self):
        self.card1 = Card(1,1)
        self.card2 = Card(4,1)
        self.card3 = Card(4,13)
        self.card4 = self.card3
        self.card5 = Card(1,1)

    #Author: Aina
    # testing suits have correct value after init
    def test_init_suits(self):
        self.assertEqual(1, self.card1.getSuit())
        self.assertEqual(4, self.card2.getSuit())
        self.assertEqual(4, self.card3.getSuit())
        self.assertNotEqual(2, self.card1.getSuit())
        self.assertNotEqual(self.card1.getSuit(), self.card3.getSuit())
        self.assertEqual(self.card2.getSuit(), self.card3.getSuit())
        self.assertEqual(self.card3.getSuit(), self.card4.getSuit())




    # testing ranks have correct value after init
    def test_init_ranks(self):
        self.assertEqual(1, self.card1.getRank())
        self.assertNotEqual(2, self.card1.getRank())
        self.assertEqual(self.card2.getRank(), self.card1.getRank())
        self.assertNotEqual(self.card2.getRank(), self.card3.getRank())
        self.assertNotEqual(2, self.card1.getRank())
        self.assertEqual(self.card3.getRank(), self.card4.getRank())


    # Author: Aina
    # testing cards are correct after init
    def test_init_card(self):
        self.assertNotEqual(self.card2, self.card1)
        #pointer
        self.assertEqual(self.card4, self.card3)
        #different cards with same values
        self.assertNotEqual(self.card1, self.card5)

if __name__ == '__main__':
    unittest.main()
