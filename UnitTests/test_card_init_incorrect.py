import unittest
from project8.Classes.Card import Card

class CardInitRaiseValueError(unittest.TestCase):
    def setUp(self):
        pass

    # Author: Aina
    # testing None inputs
    def test_init_both_no_input(self):
        with self.assertRaises(NameError) as context1:
            self.card1 = Card(None, None)
        self.assertEqual('No inputs provided', str(context1.exception))

    def test_init_suit_no_input(self):
        with self.assertRaises(NameError) as context1:
            self.card1 = Card(None, 3)
        self.assertEqual('No inputs provided', str(context1.exception))

    def test_init_rank_no_input(self):
        with self.assertRaises(NameError) as context1:
            self.card1 = Card(1, None)
        self.assertEqual('No inputs provided', str(context1.exception))

    # Author: Aina
    # testing incorrect types
    def test_init_both_incorrect_types(self):
        with self.assertRaises(TypeError) as context1:
            self.card1 = Card("A", "B")
        self.assertEqual('Input must be integers', str(context1.exception))
        with self.assertRaises(TypeError) as context1:
            self.card1 = Card([2], '')
        self.assertEqual('Input must be integers', str(context1.exception))

    def test_init_incorrect_suit(self):
        with self.assertRaises(TypeError) as context1:
            self.card1 = Card("A", 3)
        self.assertEqual('Input must be integers', str(context1.exception))
        with self.assertRaises(TypeError) as context1:
            self.card1 = Card(1.3, 3)
        self.assertEqual('Input must be integers', str(context1.exception))

    def test_init_incorrect_rank(self):
        with self.assertRaises(TypeError) as context1:
            self.card1 = Card(1, "B")
        self.assertEqual('Input must be integers', str(context1.exception))
        with self.assertRaises(TypeError) as context1:
            self.card1 = Card(4, 3.8)
        self.assertEqual('Input must be integers', str(context1.exception))

    # Author: Aina
    # testing incorrect values (not within range)
    def test_init_suit_incorrect_range(self):
        with self.assertRaises(ValueError) as context1:
            self.card1 = Card(0, 8)
        self.assertEqual('suit must be in range 1-4', str(context1.exception))
        with self.assertRaises(ValueError) as context1:
            self.card1 = Card(5, 8)
        self.assertEqual('suit must be in range 1-4', str(context1.exception))


    def test_init_rank_incorrect_range(self):
        with self.assertRaises(ValueError) as context1:
            self.card1 = Card(1, 14)
        self.assertEqual('rank must be in range 1-13', str(context1.exception))
        with self.assertRaises(ValueError) as context1:
            self.card1 = Card(3, 0)
        self.assertEqual('rank must be in range 1-13', str(context1.exception))


    def test_init_both_incorrect_range(self):
        with self.assertRaises(ValueError) as context1:
            self.card1 = Card(0, 14)
        self.assertEqual('suit must be in range 1-4', str(context1.exception))
        with self.assertRaises(ValueError) as context1:
            self.card1 = Card(5, 14)
        self.assertEqual('suit must be in range 1-4', str(context1.exception))


if __name__ == '__main__':
    unittest.main()
