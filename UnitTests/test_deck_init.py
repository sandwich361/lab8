import unittest
from project8.Classes.Deck import deck
from project8.Classes.Card import Card

#2/26 Aina Changed cards to fake cards
class TestDeckDraw(unittest.TestCase):
    class fakeCardClass:
        def __init__(self,s,r):
            if not isinstance(s, int):
                raise TypeError("suit parameter not an int")
            if not isinstance(r, int):
                raise TypeError("rank parameter not an int")
            if (s < 1 or s > 4):
                raise ValueError("suit value must be between 1 and 4")
            if (r < 1 or r > 13):
                raise ValueError("rank value must be between 1 and 14")
            self.rank = r
            self.suit = s
    #2/27 Aina changed init to setup
    def setUp(self):
        self.deck1 = deck()
        self.card0 = self.fakeCardClass(1, 1)

    def test_for_52_cards(self):
        self.assertEqual(self.deck1.getSize(), 52)

    def test_each_card_exists_in_deck(self):
        cardList = []
        for x in range(1, 5):
            for y in range(1, 14):
                self.cardi = self.fakeCardClass(x, y)
                cardList.append(self.cardi)

        for i in cardList:
            cardx = i
            exists = 'false'
            if cardx.__dict__ == self.deck1.draw().__dict__:
                exists = 'true'
                print(exists)
            self.assertTrue(exists, 'ERROR card doesnt exist in deck')


if __name__ == '__main__':
    unittest.main()