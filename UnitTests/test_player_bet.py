from django.test import TestCase
from project8.models import PlayerModel
from project8.models import PokerTableModel


class TestPlayerBet(TestCase):

    # setup for testing. will create testPlayer and testTable in the DB
    def setUp(self):
        username = "testPlayer1"
        tablename = "testTable1"
        dealername = "testDealer"

        if (PlayerModel.objects.filter(username__exact=username)):
            player = PlayerModel.objects.get(username__exact=username)
        else:
            PlayerModel.objects.create(username=username, isonline=True)
            player = PlayerModel.objects.get(username__exact=username)

        if (PokerTableModel.objects.filter(tablename__exact=tablename)):
            table = PokerTableModel.objects.get(tablename__exact=tablename)
        else:
            PokerTableModel.objects.create(tablename=tablename, dealer=dealername)
            table = PokerTableModel.objects.get(tablename__exact=tablename)

        player.chips = 100
        player.isonline = True
        table.currentBet = 0
        table.pot = 0
        table.players = player
        player.save()
        table.save()

    # ensure outerbounds for bet hold
    def test_bet_outerBounds(self, player, table):
        self.assertEquals(player.bet(self, 5, player, table), "player successfully bet 5 chips")
        self.assertNotEquals(player.bet(self, 105, player, table), "player successfully bet 105 chips")
        self.assertEquals(player.bet(self, 105, player, table), "Error: bad bet chips value")
        self.assertNotEquals(player.bet(self, -5, player, table), "player successfully bet -5 chips")

    # ensure bad value inputs for chips are caught
    def test_bet_badChipsInput(self, player, table):
        self.assertEquals(player.bet(self, 5, player, table), "player successfully bet 5 chips")
        self.assertNotEquals(player.bet(self, "5", player, table), "player successfully bet 5 chips")
        self.assertNotEquals(player.bet(self, "abc", player, table), "player successfully bet abc chips")

    # ensure bad usernames don't return successfully
    def test_bet_badUserName(self, player, table):
        self.assertEquals(player.bet(self, 5, player, table), "player successfully bet 5 chips")
        self.assertNotEquals(player.bet(self, 5, "playerz", table), "player successfully bet 5 chips")
        self.assertEquals(player.bet(self, 5,"playerz", table), "Error: playerz does not exist")

    # ensure bad tablenames don't return successfully
    def test_bet_badTableName(self, player, table):
        self.assertEquals(player.bet(self, 5, player, table), "player successfully bet 5 chips")
        self.assertNotEquals(player.bet(self, 5, player, "tablez"), "player successfully bet 5 chips")
        self.assertEquals(player.bet(self, 5, player, "tablez"), "Error: tablez does not exist")

    # ensure table pot field updated correctly
    def test_bet_tablePot(self, player, table):
        table.pot = 0
        player.chips = 100
        table.save()
        player.save()
        self.assertEquals(table.pot, 0)
        player.bet(self, 5, player, table)
        self.assertEquals(table.pot, 5)
        self.assertNotEquals(table.pot, 0)

    # ensure table currentBet field updated correctly
    def test_bet_tableCurrentBet(self, player, table):
        table.currentBet = 0
        player.chips = 100
        table.save()
        player.save()
        self.assertEquals(table.currentBet, 0)
        player.bet(self, 10, player, table)
        self.assertEquals(table.currentBet, 10)
        self.assertNotEquals(table.currentBet, 0)
        player.bet(self, 5, player, table)
        self.assertEquals(table.currentBet, 5)
        self.assertNotEquals(table.currentBet, 10)
        self.assertNotEquals(table.currentBet, 0)

    # ensure player chips are updated correctly
    def test_bet_playerChips(self, player, table):
        player.chips = 100
        player.save()
        self.assertEquals(player.chips, 100)
        player.bet(self, 5, player, table)
        self.assertEquals(player.chips, 95)
        player.bet(self, 15, player, table)
        self.assertEquals(player.chips, 80)
        self.assertEquals(player.bet(self, 85, player, table), "Error: bad bet chips value")
        self.assertNotEquals(player.bet(self, 85, player, table), "player successfully bet 85 chips")
