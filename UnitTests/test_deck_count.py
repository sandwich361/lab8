import unittest
from project8.Classes import Card, Deck


#Author: Ridaa
#2/17/20 Ridaa finished test file for deck_count
class TestDeckCount(unittest.TestCase):

    def setUp(self):

        self.deck1 = Deck()
        self.deck2 = Deck()
        self.deck3 = Deck()


    #this method is raising a TypeError if count does not return an integer
    #2/27 Aina changed Integer to int
    def test_count_return_incorrect(self):
        if not isinstance(self.deck1.count(), int):
            raise TypeError('count is not returning an integer')
        if not isinstance(self.deck2.count(), int):
            raise TypeError('count is not returning an integer')
        if not isinstance(self.deck3.count(), int):
            raise TypeError('count is not returning an integer')

    #this method makes sure a Value Error is raised when count returns an invalid integer. (if it's <0 or >52)
    def test_count_invalid_int(self):
        #for every instance of draw, make sure count is not an invalid integer
        for count in range(0,52):
            self.deck1.draw()
            i1 = self.deck1.count()
            if i1>52 | i1<0:
                raise ValueError('count must return an integer between 0 and 52')

            self.deck2.draw()
            i2 = self.deck2.count()
            if i2 > 52 | i2 < 0:
                raise ValueError('count must return an integer between 0 and 52')

            self.deck3.draw()
            i3 = self.deck3.count()
            if i3 > 52 | i3 < 0:
                raise ValueError('count must return an integer between 0 and 52')

    #this method will call draw a card 52 times and make sure count returns correct value after every draw
    def test_correct_count_deck(self):
        #when no cards are drawn, count should return 52
        self.assertEqual(self.deck1.count(),52)
        for x in range(1,52):
            self.deck1.draw()
            count = 52-x
            self.assertEqual(self.deck1.count(), count)

    #this method will draw a certain amount of cards in each deck and make sure count doesn't return the wrong
    #integer
    def test_incorrect_count_deck(self):
        #drew 5 cards from deck1, count can't return anything other than 47
        for a in range(1,6):
            self.deck1.draw()
        for a1 in range(0,46):
            self.assertNotEqual(self.deck1.count(), a1)
        for a2 in range(48,53):
            self.assertNotEqual(self.deck1.count(), a2)

        #drew 16 cards from deck2, count shouldn't return any integer other than 36
        for b in range(1,17):
            self.deck1.draw()
        for b1 in range(1,36):
            self.assertNotEqual(self.deck2.count(), b1)
        for b2 in range(37,52):
            self.assertNotEqual(self.deck2.count(), b2)

        #did not draw any cards from deck 3, count can't return anything other than 52
        for c in range(1,52):
            self.assertNotEqual(self.deck3.count(), c)




if __name__ == '__main__':
    unittest.main()
