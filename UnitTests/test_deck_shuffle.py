import unittest
from project8.Classes.Deck import deck

# author - Noah Ratzburg
# date   - 2/26/2020
# I am assuming the deck class will contain a list containing cards and a dictionary mapping tuple of (suit, rank) to bool values
# A shuffle will set all values of deck to false indicating they are in the being shuffled
# Then I assume we will randomly generate a number between 1 and 52.
# Then I calculate the rank and suit with this algorithm and check if it is in the deck.
# ((rand%4), (rand%13))
class TestDeckShuffle(unittest.TestCase):

    def setUp(self):
        self.deck1 = deck()
        self.deck2 = self.deck1


# author - Noah Ratzburg
# date   - 2/27/2020
# testing shuffle. initialize one deck and set another to a copy of the second
# deck can shuffle to be the same as previous shuffle, very small chance
# for loop will handle that, as long as it shuffles differently at least once
# the case is sound
    def test_shuffle(self):
        self.deck1.shuffle()
        count = 0
        for i in range(1, 52):
            if (self.deck1 == self.deck2):
                count += 1
                self.deck1.shuffle()
        self.assertTrue(count <= 52)
    # Only one test is necessary as the remainder of the exceptions can be handled in the test_deck_init


    #def test_shuffle_valid(self):
    #    count = 0
        # checking that the value is true, it is in the deck then checking to make sure there isnt more than one of the
        # same card in a deck
    #    for k, v in self.deck1.deck.items():
    #        self.assertTrue(v, "error: card not in deck")
    #        self.assertEquals(self.deck1.card.count(k), 1, "error: invalid count of card in deck")
    #        count += 1
    #    self.assertEqual(count, 52, "error: too many keys!")

        # checking every single card in the deck and asserting the value is true, meaning it is in the deck
    #    for c in self.deck1.cards:
    #        self.assertTrue(self.deck1.deck[(c.getSuit(), c.getRank())])
            
        