from django.test import TestCase
from project8.models import PlayerModel
from project8.models import PokerTableModel, AdminModel, DealerModel


class TestPlayerJoin(TestCase):

    # setup for testing. will create testPlayer and testTable in the DB
    def setUp(self):
        AdminModel.objects.create(username="AdminSandwich", isonline=True)

        # mutliple tables
        dealer = DealerModel.objects.create(username="MasterSandwich", isonline=True)
        PokerTableModel.objects.create(name="table1", dealer=dealer)
        megadealer = DealerModel.objects.create(username="MegaSandwich", isonline=True)
        PokerTableModel.objects.create(name="table2", dealer=megadealer)

        # players
        self.player1 = PlayerModel.objects.create(username="player1", isonline=True)
        self.player2 = PlayerModel.objects.create(username="player2", isonline=True)
        self.player3 = PlayerModel.objects.create(username="player3", isonline=True)
        self.player4 = PlayerModel.objects.create(username="player4", isonline=True)
        self.player5 = PlayerModel.objects.create(username="player5", isonline=True)


    def test_player_join_good(self):
        self.assertEqual(self.player1.join_game("table1"), "player1 successfully joined table1")
        self.player1.join_game("table1")
        self.table1.save()
        self.player1.save()
        self.assertEquals(1, len(self.table1.players))
        self.player2.join_game("table2")
        self.table2.save()
        self.player2.save()
        self.assertEquals(2, len(self.table1.players))

    def test_wrong_table_name(self):
        self.assertEqual(self.player1.join_game("tablee1"), "cannot join because table1 does not exist")

    def test_player_already_in(self):
        self.player1.join_game("table1")
        self.table1.save()
        self.player1.save()
        self.assertEqual(self.player1.join_game("table1"), "player1 is already in table1")

    def test_table_full(self):
        self.table1.players = [self.player1, self.player2, self.player3, self.player4]
        self.table1.save()
        self.assertEqual(self.player5.join_game("table1"), "table1 cannot add more players")