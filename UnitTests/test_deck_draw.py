import unittest
from project8.Classes.Deck import deck
from project8.Classes.Card import Card

#Author: Ridaa
#2/17/20 Ridaa created unit test file for deck_draw
class TestDeckDraw(unittest.TestCase):
    class fakeCardClass:
        def __init__(self,s,r):
            if not isinstance(s, int):
                raise TypeError("suit parameter not an int")
            if not isinstance(r, int):
                raise TypeError("rank parameter not an int")
            if (s < 1 or s > 4):
                raise ValueError("suit value must be between 1 and 4")
            if (r < 1 or r > 13):
                raise ValueError("rank value must be between 1 and 14")
            self.rank = r
            self.suit = s
    def setUp(self):

        self.deck1 = deck()
        self.deck2 = deck()
        self.deck3 = deck()

        # initialize sorted deck

        self.sorted_deck = []
        for x in range(1, 5):
            for y in range(1, 14):
                self.fakeCard = self.fakeCardClass(x,y)
                self.sorted_deck.append(self.fakeCard)

         # initialize somewhat random deck
         # [ 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 2,
         # 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52]
        self.random_deck = [];
        for a in range(0, 52, 2):
            self.random_deck.append(self.sorted_deck[a])
            for b in range(1, 52, 2):
                self.random_deck.append(self.sorted_deck[b])

        #intilialize somewhat random deck from random_deck1
        self.random_deck2 = []
        # [1, 5, 9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 2, 6, 10, 14, 18, 22, 26, 30, 34, 38, 42, 46, 50, 3, 7, 11,
        # 15, 19, 23, 27, 31, 35, 39, 43, 47, 51, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52]
        for a in range(0, 52, 2):
            self.random_deck2.append(self.random_deck[a])
        for b in range(1, 52, 2):
            self.random_deck2.append(self.random_deck[b])

    #2/26 Aina changed Card to self.fakeCard
    #this method is raising an error if draw doesnt return a card
    # def test_draw_return_type_incorrect(self):
    #     print(not isinstance(self.deck1.draw(), self.fakeCardClass))
    #     if not isinstance(self.deck1.draw(), self.fakeCardClass):
    #         raise TypeError('draw should only return a Card')
    #     if not isinstance(self.deck2.draw(),self.fakeCard):
    #         raise TypeError('draw should only return a Card')
    #     if not isinstance(self.deck3.draw(), self.fakeCard):
    #         raise TypeError('draw should only return a Card')


    #makes sure decks 1,2,& 3 return the right card based on which draw() we are on

    #assume sorted_deck is the card arrangement for deck1
    def test_deck1_correct(self):
        for a in range(51,0, -1):
            self.fakeCard1 = self.deck1.draw()
            self.assertEqual(self.sorted_deck[a].rank, self.fakeCard1.getRank())
            self.assertEqual(self.sorted_deck[a].suit, self.fakeCard1.getSuit())

    #assume random_deck1 is the card arrangement for deck2
    def test_deck2_correct(self):
        for b in range(51, 0, -1):
            self.fakeCard2 = self.deck2.draw()
            print(str(self.fakeCard2.rank) +" "+ str(self.fakeCard2.suit))
            self.assertEqual(self.random_deck[b].rank, self.fakeCard2.getRank())
            self.assertEqual(self.random_deck[b].suit, self.fakeCard2.getSuit())


    #assume random_deck2 is the card arrangement for deck3
    def test_deck_3_correct(self):
        for c in range(51, 0, -1):
            self.fakeCard3 = self.deck3.draw()
            self.assertEqual(self.random_deck2[c].rank, self.fakeCard3.getRank())
            self.assertEqual(self.random_deck2[c].suit, self.fakeCard3.getSuit())


    #makes sure deck doesn't return a card that was already drawn
    def test_deck_1_incorrect(self):
        for a1 in range(0,52):
            self.fakeCard1 = self.deck1.draw()
            for a2 in range(0,a1):
                self.fakeCardtmp = self.sorted_deck[a2]
                self.assertNotEqual(self.fakeCardtmp,self.fakeCard1)

    #makes sure deck doesn't return a card under the card at the top
    def test_deck2_incorrect(self):
        for b1 in range(0,52):
            self.fakeCard1 = self.deck1.draw()
            for b2 in range(b1+1,53):
                self.fakeCardtmp = self.random_deck[b2]
                self.assertNotEqual(self.fakeCardtmp, self.fakeCard1)




if __name__ == '__main__':
    unittest.main()
