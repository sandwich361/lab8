from django.db import models
# Create your models here.

class User(models.Model):
    username = models.CharField(max_length=20)
    password = models.CharField(max_length=20, default="password")


class AdminModel(models.Model):
    username = models.CharField(max_length=20)
    password = models.CharField(max_length=20, default="password")
    # associated_user = models.ForeignKey(User, on_delete=models.CASCADE)

    isonline = models.BooleanField(default=False)
    def __str__(self):
        return self.username
    def login(self):
        pass
    def create_dealer(self, username):
        pass
    def create_table(self, tablename):
        pass
    def create_player(self, username):
        pass
    def assign_player_to_table(self, username):
        pass
    def reset_score(self):
        pass


class CardModel(models.Model):
    suit = models.IntegerField(default=1)
    rank = models.IntegerField(default=1)
    # 1:clubs 2:spades 3:hearts 4:diamonds
    def getSuit(self):
        pass
    def getRank(self):
        pass
    def getValue(self):
        pass
    def __str__(self):
        return "Suit:"+str(self.suit)+" Rank:"+str(self.rank)

    def equalsCard(self):
        pass

class DeckModel(models.Model):
    name = models.CharField(max_length=10, default="deck")
    cards = models.ManyToManyField(CardModel)
    #def __init__(self):
        # create a standard 52 card deck of cards
     #   pass
    def count(self):
        # return the number of cards remaining in the deck
        pass
    def draw(self):
        # return and remove the top card in the deck
        # if the deck is empty, raise a ValueError
        pass
    def shuffle(self):
        # shuffle the deck using a random number generator
        pass
    def getSize(self):
        # size of deck
        pass

    def __str__(self):
        return self.name

class PlayerModel(models.Model):
    username = models.CharField(max_length=20)
    password = models.CharField(max_length=20, default="password")
    # associated_user = models.ForeignKey(User, on_delete=models.CASCADE)
    hand = models.ManyToManyField(CardModel)
    highscore = models.IntegerField(default=0)
    chips = models.IntegerField(default=100)
    betAmount = models.IntegerField(default=0)
    isonline = models.BooleanField(default=False)
    folded = models.BooleanField(default=False)

    def __str__(self):
        return self.username
    def bet(self, bet_chips, player_username, player_table):
        pass
    def call(self):
        pass
    def fold(self):
        pass
    def raisebet(self):
        pass
    def seechips(self):
        pass
    def allin(self):
        pass


class DealerModel(models.Model):
    username = models.CharField(max_length=20)
    password = models.CharField(max_length=20, default="password")
    # associated_user = models.ForeignKey(User, on_delete=models.CASCADE)
    isonline = models.BooleanField(default=False)
    deck = models.ForeignKey(DeckModel, on_delete=models.CASCADE)

    def __str__(self):
        return self.username
    def draw(self):
        pass


class PokerTableModel(models.Model):
    name = models.CharField(max_length=15)
    pot = models.IntegerField(default=0)
    currentBet = models.IntegerField(default=0)
    dealer = models.ForeignKey(DealerModel, on_delete=models.CASCADE)
    players = models.ManyToManyField(PlayerModel)
    gameStarted = models.BooleanField(default=False)
    def __str__(self):
        return self.name



