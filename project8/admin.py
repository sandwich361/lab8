from django.contrib import admin
from project8.models import PlayerModel, DealerModel, PokerTableModel, AdminModel, CardModel, DeckModel, User



# Register your models here.
admin.site.register(User)
admin.site.register(AdminModel)
admin.site.register(DealerModel)
admin.site.register(PokerTableModel)
admin.site.register(PlayerModel)
admin.site.register(CardModel)
admin.site.register(DeckModel)
