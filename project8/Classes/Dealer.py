from project8.Classes.Deck import deck
from project8.models import DealerModel, DeckModel, PokerTableModel, CardModel, PlayerModel
from project8.Classes.Card import Card
from project8.Classes.HandTypes import HandTypes


class Dealer:

    def __init__(self, username):
        self.isonline = True
        self.username = username
        self.deck = deck() #new deck for dealer

        if self.username is None:
            raise NameError('Must provide username to create dealer')
        if not isinstance(self.username, str):
            raise TypeError('Incorrect data type provided for username')

    def deal_cards(self,table_name, deck):
        try:
            table = PokerTableModel.objects.get(name__exact=table_name)
        except PokerTableModel.DoesNotExist:
            table = None
        if table:
            if (table.players.count() < 2):
                return "not enough players in table to start game"
            else:
                self.deck.shuffle()
                list = table.players.all()
                for player in list:
                    if player.hand.all().count() >= 5:
                        return "could not deal cards: players have cards"
                    for i in range(5):
                        card = self.deck.draw()
                        playerCard = CardModel.objects.create(suit=card.getSuit(), rank=card.getRank())
                        player.save()
                        player.hand.add(playerCard)
                # for player in list:
                #     print(player.hand.all())
                return "Dealer successfully dealed cards"
        else:
            return "could not deal cards: table does not exist"






    def deal(self, table_players):
        if table_players is None:
            raise NameError('Must provide a list of players to deal to')
        if not isinstance(table_players, list):
            raise TypeError('Argument is not of type list')
        count = len(table_players)
        if count < 1 or count > 5:
            raise ValueError('Must provide a list of 1-5 players')
        for i in range(5):
            for player in table_players:

                card = self.deck.draw()
                # deck.save()
                # if not isinstance(player, Player):
                #     raise TypeError("item in table_players not a Player object")
                # player.hand.append(card)
                player.cards.append(card)
                # player.save()


    # test to see if able to end round:
    # all active players must
    def flop(self, dealer_name, dealer_table):
        if (dealer_name and dealer_table):
            if (PlayerModel.objects.filter(username__exact=dealer_name).count == 0):
                return "Error: " + dealer_name + " does not exist"
            elif (PokerTableModel.objects.filter(name__exact=dealer_table).count == 0):
                return "Error: " + dealer_table + " does not exist"
            else:
                dealer = DealerModel.objects.get(username__exact=dealer_name)
                table = PokerTableModel.objects.get(name__exact=dealer_table)

            # ensure someone bet something this round
            testIfBet = False
            for player in table.players.all():
                if(player.betAmount != 0):
                    testIfBet = True
            if(testIfBet == False):
                return "Error: a player must bet before the flop"

            # ensure every player at the table bet the same unless they folded
            neededAmount = table.players.first().betAmount
            for player in table.players.all():
                if(player.folded != True):
                    if player.betAmount != neededAmount:
                        return "Error: every player must have bet the same amount or folded"

            # call command to calculate hands
            handValues = {}
            for player in table.players.all():
                if(player.folded == False):
                    handValues[str(player.username)] = int(HandTypes.checkhandtype(self, player.hand))
            # TODO
            #  at beginning of game, players are all folded, so handValues will be null.
            # set folded to false before we reach this line of
            # error check
            # if (len(handValues) ==0):
            #     return "all players are folded"
            winningPlayer = max(handValues, key=handValues.get)
            print(winningPlayer)

            # call command to reset needed fields (folded, bet, table currentbet)
            # also payout winner
            for player in table.players.all():
                # give winner pot and reset
                if player.username == winningPlayer:
                    player.betAmount = 0
                    player.chips += table.pot
                    player.hand.clear()
                    player.save()
                # reset non winners without pot
                else:
                    player.betAmount = 0
                    player.folded = False
                    player.hand.clear()
                    player.save()

            # reset table to beginning of game
            table.currentBet = 0
            table.pot = 0
            table.save()

            # deal out another next hand
            dealer = DealerModel.objects.get(username=dealer_name)
            table_name = dealer_table
            dealer_instance = Dealer(dealer.username)
            dealer_instance.deal_cards(table_name, dealer.deck)
            return winningPlayer + " won the hand! " + "New Hand Delt!"
        else:
            return "Error: need dealer name and table"
