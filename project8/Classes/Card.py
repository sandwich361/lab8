class Card:
    def __init__(self, s, r):
      if not isinstance(s, int):
        raise TypeError("suit parameter not an int")
      if not isinstance(r, int):
        raise TypeError("rank parameter not an int")
      if (s<1 or s>4):
        raise ValueError("suit value must be between 1 and 4")
      if (r<1 or r>13):
        raise ValueError("rank value must be between 1 and 14")
      self.rank = r
      self.suit = s
    # initialize a card to the given suit (1-4) and rank (1-13)
    # 1:clubs 2:spades 3:hearts 4:diamonds
    # raise an exception for invalid argument, ValueError
    def getSuit(self):
        # return the suit of the card (1-4)
        if (self.suit < 1 or self.suit > 4):
            raise ValueError('cannot get suit of card')
        return self.suit
    def getRank(self):
        # return the rank of the card (1-13)
        if (self.rank < 1 or self.rank > 13):
            raise ValueError('cannot get rank of card')
        return self.rank
    def getValue(self):
    #get the game-specific value of a Card
    #if not used, or game is not defined, always returns 0.
        pass
    def __str__(self):
    #return rank and suite, as AS for ace of spades, or 3H for
    #three of hearts, JC for jack of clubs.
    # 1:clubs 2:spades 3:hearts 4:diamonds
        name = ""
        if self.rank == 1:
            name += "A"
        elif self.rank == 11:
            name += "J"
        elif self.rank == 12:
            name += "Q"
        elif self.rank == 13:
            name += "K"
        else:
            name += str(self.rank)


        if self.suit == 1:
          name += "C"
        elif self.suit == 2:
          name += "S"
        elif self.suit == 3:
          name += "H"
        else:
          name += "D"


        return name

    def equalsCard(self):
      pass