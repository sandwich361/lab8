from project8.models import AdminModel, PlayerModel, PokerTableModel, DealerModel
from project8.Classes.AdminClass import Admins
from project8.Classes.Player import Player
from project8.Classes.Dealer import Dealer

class Command():

    message = ''
    allcommands = []
    print(allcommands)
    def check_command(self, command):

        command_args_list = command.split(":")
        username_check = command_args_list[0]
        command_check = command_args_list[1]

        #admin commands
        if 'login' in command_check:
            response = self.login_handler(username_check)
            self.allcommands.append(response)
        elif 'createplayer' in command_check:
            response = self.create_player_handler(username_check, command_check)
            self.allcommands.append(response)
        elif 'createdealer' in command_check:
            response = self.create_dealer_handler(username_check, command_check)
            self.allcommands.append(response)
        elif 'createtable' in command_check:
            response = self.create_table_handler(username_check, command_check)
            self.allcommands.append(response)
        elif 'resetscore' in command_check:
            response = self.reset_score_handler(username_check, command_check)
            self.allcommands.append(response)
        elif 'join' in command_check:
            response = self.player_join_handler(username_check, command_check)
            self.allcommands.append(response)
        elif 'raisebet' in command_check:
            response = self.player_raisebet_handler(username_check, command_check)
            self.allcommands.append(response)
        elif 'bet' in command_check:
            response = self.player_bet_handler(username_check, command_check)
            self.allcommands.append(response)
        elif 'seechips' in command_check:
            response = self.player_seechips_handler(username_check, command_check)
            self.allcommands.append(response)
        elif 'call' in command_check:
            response = self.player_call_handler(username_check, command_check)
            self.allcommands.append(response)
        elif 'allin' in command_check:
            response = self.player_allin_handler(username_check, command_check)
            self.allcommands.append(response)
        elif 'deal' in command_check:
            response = self.dealer_deal_handler(username_check, command_check)
            self.allcommands.append(response)
        elif 'flop' in command_check:
            return self.dealer_flop_handler(username_check, command_check)
        elif 'fold' in command_check:
            return self.player_fold_handler(username_check, command_check)
        else:
            return "please type in correct command"

        self.allcommands.reverse()

        return response

    def login_handler(self, admin_username):
        #check correct username
        if admin_username == "":
            return 'Error: Cannot login. No username provided'
        try:
            admin = AdminModel.objects.get(username__exact=admin_username)
        except AdminModel.DoesNotExist:
            admin = None

        if admin:
            admins_instance = Admins(admin.username, admin.isonline)
            Command.message = admins_instance.make_admin_online(admin_username)
        else:
            Command.message = "Error: Admin \""+ admin_username + "\" cannot login"
        return Command.message

    def create_player_handler(self, admin_username, command_check):
        #check correct admin username
        if admin_username == "":
            return 'Error: No username provided'
        try:
            admin = AdminModel.objects.get(username__exact=admin_username)
        except AdminModel.DoesNotExist:
            admin = None
        #parsing message
        command_args_arr = command_check.split()
        print(command_args_arr)
        create_player_command = command_args_arr[0]
        if create_player_command != 'createplayer':
            return "please type in correct command"
        elif len(command_args_arr) > 2 or len(command_args_arr) == 1 :
            return "Please type in 1 player name"

        player_username = command_args_arr[1]

        if admin:
            Command.message= Admins.create_player(self, player_username)
        else:
            Command.message = "Error: Admin \""+ admin_username + "\" cannot create player"
        return Command.message

    def create_dealer_handler(self, admin_username, command_check):
        # check correct admin username
        if admin_username == "":
            return 'Error: No username provided'
        try:
            admin = AdminModel.objects.get(username__exact=admin_username)
        except AdminModel.DoesNotExist:
            admin = None
        # parsing message
        command_args_arr = command_check.split()
        print(command_args_arr)
        create_dealer_command = command_args_arr[0]
        if create_dealer_command != 'createdealer':
            return "please type in correct command"
        elif len(command_args_arr) > 2 or len(command_args_arr) == 1:
            return "Please type in 1 dealer name"

        dealer_username = command_args_arr[1]

        if admin:
            Command.message = Admins.create_dealer(self, dealer_username)
        else:
            Command.message = "Error: Admin \"" + admin_username + "\" cannot create dealer"

        return Command.message

    def create_table_handler(self, admin_username, command_check):
        # check correct admin username
        if admin_username == "":
            return 'Error: No username provided'
        try:
            admin = AdminModel.objects.get(username__exact=admin_username)
        except AdminModel.DoesNotExist:
            admin = None

        # parsing message
        command_args_arr = command_check.split()
        print(command_args_arr)
        create_table_command = command_args_arr[0]
        if create_table_command != 'createtable':
            return "please type in correct command"
        elif len(command_args_arr) > 2 or len(command_args_arr) == 1:
            return "Please type in 1 table name"

        table_name = command_args_arr[1]

        if admin:
            Command.message = Admins.create_table(self, table_name)
        else:
            Command.message = "Error: Admin \"" + admin_username + "\" cannot create table"

        return Command.message

    def reset_score_handler(self, admin_username, command_check):
        # check correct admin username
        if admin_username == "":
            return 'Error: No username provided'
        try:
            admin = AdminModel.objects.get(username__exact=admin_username)
        except AdminModel.DoesNotExist:
            admin = None
        # parsing message
        command_args_arr = command_check.split()
        print(command_args_arr)
        create_table_command = command_args_arr[0]
        if create_table_command != 'resetscore':
            return "please type in correct command"
        elif len(command_args_arr) != 2:
            return "please choose a player"
        player_username = command_args_arr[1]

        if admin:
            Command.message = Admins.reset_score(self,playername=player_username)
        else:
            Command.message = "Error: Admin \"" + admin_username + "\" cannot create table"

        return Command.message

    def player_join_handler(self, username_check, command_check):
        # check correct username
        if username_check == "":
            return 'Error: Cannot join. No username provided'
        try:
            player = PlayerModel.objects.get(username__exact=username_check)
        except PlayerModel.DoesNotExist:
            player = None

        #parse message
        command_args_arr = command_check.split()
        join_command = command_args_arr[0]
        if join_command != 'join':
            return "join command written incorrectly"
        elif len(command_args_arr) != 2:
            return "please type in 1 tablename"
        table_name = command_args_arr[1]

        if player:
            if player.isonline:
                player_instance = Player(username=username_check)
                Command.message = player_instance.join_game(tablename=table_name)
            else:
                return 'cannot join because player does not exist'
        else:
            Command.message = 'cannot join because player does not exist'
        return Command.message

    def player_bet_handler(self, player_username, command_check):
        try:
            player = PlayerModel.objects.get(username=player_username)
        except PlayerModel.DoesNotExist:
            player = None
        if player:
            command_args_arr = command_check.split()
            bet_command = command_args_arr[0]
            if bet_command != 'bet':
                return "bet command written incorrectly"
            elif  len(command_args_arr) != 3:
                return "command must only include 'bet' <amount> <name of table>"
            else:
                bet_amount = command_args_arr[1]
                try:
                    num = int(bet_amount)
                except ValueError:
                    return "bet amount not an integer"
                table_name = command_args_arr[2]
                bet_message = Player.bet(self, num, player_username, table_name)
                return bet_message
        else:
            return "can't bet because player does not exist"

    def player_seechips_handler(self, player_username, command_check):
        try:
            player = PlayerModel.objects.get(username=player_username)
        except PlayerModel.DoesNotExist:
            player = None
        if player:
            command_args_arr = command_check.split()
            seechips_command = command_args_arr[0]
            if seechips_command != 'seechips':
                return "seechips command written incorrectly"
            elif len(command_args_arr) != 2:
                return "command must only include 'seechips' <name of table>"
            else:
                table_name = command_args_arr[1]
                seechips_message = Player.seechips(self, player_username, table_name)
                return seechips_message
        else:
            return "can't see chips because player does not exist"


    def player_raisebet_handler(self, player_username, command_check):
        try:
            player = PlayerModel.objects.get(username=player_username)
        except PlayerModel.DoesNotExist:
            player = None
        if player:
            command_args_arr = command_check.split()
            raisebet_command = command_args_arr[0]
            if raisebet_command != 'raisebet':
                return "raisebet command written incorrectly"
            elif len(command_args_arr) != 3:
                return "command must only include 'raisebet' <amount> <name of table>"
            else:
                bet_amount = command_args_arr[1]
                try:
                    num = int(bet_amount)
                except ValueError:
                    return "raise bet amount not an integer"
                table_name = command_args_arr[2]
                raisebet_message = Player.raisebet(self, num, player_username, table_name)
                return raisebet_message
        else:
            return "can't raise bet because player does not exist"

    def player_call_handler(self, player_username, command_check):
        try:
            player = PlayerModel.objects.get(username=player_username)
        except PlayerModel.DoesNotExist:
            player = None
        if player:
            command_args_arr = command_check.split()
            call_command = command_args_arr[0]
            if call_command != 'call':
                return "call command written incorrectly"
            elif len(command_args_arr) != 2:
                return "command must only include 'call' <name of table>"
            else:
                table_name = command_args_arr[1]
                call_message = Player.call(self, player_username, table_name)
                return call_message
        else:
            return "can't call because player does not exist"

    def player_allin_handler(self, player_username, command_check):
        try:
            player = PlayerModel.objects.get(username=player_username)
        except PlayerModel.DoesNotExist:
            player = None
        if player:
            command_args_arr = command_check.split()
            allin_command = command_args_arr[0]
            if allin_command != 'allin':
                return "allin command written incorrectly"
            elif len(command_args_arr) != 2:
                return "command must only include 'allin' <name of table>"
            else:
                table_name = command_args_arr[1]
                allin_message = Player.allin(self, player_username, table_name)
                return allin_message
        else:
            return "can't go all in because player does not exist"

    def dealer_flop_handler(self, dealer_name, command_check):
        try:
            dealer = DealerModel.objects.get(username=dealer_name)
        except DealerModel.DoesNotExist:
            dealer = None

        if dealer:
            command_args_arr = command_check.split()
            flop_command = command_args_arr[0]
            if flop_command != 'flop':
                return "flop command written incorrectly"
            elif len(command_args_arr) != 2:
                return "command must only include 'flop' <name of table>"
            else:
                table = command_args_arr[1]
                flop_message = Dealer.flop(self, dealer.username, table)
                return flop_message
        else:
            return "Error: " + dealer_name + " does not exist"

    def player_fold_handler(self, player_username, command_check):
        player = PlayerModel.objects.get(username=player_username)
        if player:
            command_args_arr = command_check.split()
            flop_command = command_args_arr[0]
            if flop_command != 'fold':
                return "fold command written incorrectly"
            elif len(command_args_arr) != 2:
                return "command must only include 'fold' <name of table>"
            else:
                table_name = command_args_arr[1]
                fold_message = Player.fold(self, player_username, table_name)
                return fold_message
        else:
            return "can't fold because player does not exist"
    def dealer_deal_handler(self, dealer_username, command_check):
        try:
            dealer = DealerModel.objects.get(username=dealer_username)
        except DealerModel.DoesNotExist:
            dealer = None


        if dealer:
            command_args_arr = command_check.split()
            deal_command = command_args_arr[0]
            if deal_command != 'deal':
                return "deal command written incorrectly"
            elif len(command_args_arr) != 2:
                return "command must only include 'deal' <name of table>"

            else:
                table_name = command_args_arr[1]
                dealer_instance = Dealer(dealer.username)
                return dealer_instance.deal_cards(table_name, dealer.deck)

        else:
            return "can't deal because dealer does not exist"


