from project8.models import PokerTableModel, PlayerModel,DealerModel, CardModel, DeckModel
from project8.Classes.Player import Player
from project8.Classes.HandTypes import HandTypes

class PokerTable:

    def __init__(self, name, dealer, players):
        self.name = name
        self.pot = 0
        self.dealer = dealer
        self.players = players
        self.currentBet = 0
        self.activePlayers = players
        self.foldedPlayers = None
        self.playerBets = {}

        # add all players to dictionary to store their bet values
        for player in players:
            self.playerBets[player] = 0

    # this method will call the methods in a loop until one player has all of the chips
    # this method will call the methods in a loop until one player has all of the chips
    # possible exit condition could also be if the admin ends the game
    # in that case the player with the highest chip count would win
    def game_driver(self):
        # call: game initialize
        message = self.game_initialize(self)
        if message == "not enough players":
            return "can not start game, not enough players"
        # loop: while no player has all chips / admin didn't end the game
        table = PokerTableModel.objects.get(name=self.name)
        players = table.players
        maxchips = table.players.count() * 100
        while players.count() > 1:  # admin ends game
            # loop to check if one plasyer has all chips
            for player in players:
                if player.chips == maxchips:
                    return "the winner is " + player
            #   call: game deal hands
            self.game_dealHands(self)
            #   call: game betting
            self.game_betting(self)
            #   call: game hand winner
            lag = None
            topplayer = None
            for player in players:
                if lag == None:
                    lag = player
                else:
                    laghandtype = HandTypes.checkhandtype(self, lag.cards)
                    playerhandtype = HandTypes.checkhandtype(self, player.cards)
                    if laghandtype > playerhandtype:
                        topplayer = lag
                    else:
                        topplayer = player
                    lag = player
            return topplayer
        # end loop
        # call: game winner

    # method will initialize fields required for the game once a min of two players are at a table
    def game_initialize(self, tablename):
        #check 2 players are at table (pokertablemodel)
        #create instance of this table with the players & dealer
        table = PokerTableModel.objects.get(name=tablename)
        if table.players.count() < 2:
            return "not enough players"

        player_at_table = table.players.all()
        players = []
        for player in player_at_table:
            player = Player(player.username)
            players.append(player)

        self.__init__(self, table.name, table.dealer, players)
        return "table created " + self.__str__(self)

    # will loop through players dealing a hand or new hand
    def game_dealHands(self):
        if len(self.activePlayers) > 1:
            self.dealer.take_cards(self.activePlayers)
            self.dealer.deck.shuffle()
            self.dealer.deal(self.activePlayers)
        else:
            raise ValueError('not enough active players to deal hand to')
    # loop through players allowing them to bet
    # go around the table allowing players to bet
    # maybe print a message to the screen saying "player1's turn"
    # that player must bet something or fold that bet is now the table current bet
    # each subsequent player must either call the current bet, raise the bet higher, or fold
    # keep looping around the table until every player has folded or bet the table current bet
    def game_betting(self):
        
        pass

    def __str__(self):
        playerlistStr = '\n'.join([player.username for player in self.players])
        return "table: "+self.name + "\ndealer: "\
               +self.dealer.username + "\nplayers:\n" +playerlistStr \
               + "\nfolded players:\n"
