from project8.models import PlayerModel
from project8.models import PokerTableModel
from project8.Classes.PokerGame import PokerGame


class Player:
    def __init__(self, username):
        self.username = username
        self.highscore = 0
        self.isonline = False
        self.chips = 100
        self.betAmount = 0
        self.cards = []

    def seechips(self, player_username, player_table):
        if (player_username and player_table):
            if (PlayerModel.objects.filter(username__exact=player_username) == None):
                return "Error: " + player_username + " does not exist"
            elif (PokerTableModel.objects.filter(name__exact=player_table) == None):
                return "Error: " + player_table + " does not exist"
            else:
                player = PlayerModel.objects.get(username__exact=player_username)
                table = PokerTableModel.objects.get(name__exact=player_table)
                if player in table.players.all():
                    return "Player " + player_username + " has " + str(player.chips) + " chips"
                else:
                    return "Player" + player_username + " is not a part of table " + table.name
        else:
            return "Error: need player username and table"


    def bet(self, bet_chips, player_username, player_table):
        if(player_username and player_table):
            if (PlayerModel.objects.filter(username__exact=player_username) == None):
                return "Error: " + player_username + " does not exist"
            elif(PokerTableModel.objects.filter(name__exact=player_table) == None):
                return "Error: " + player_table + " does not exist"
            else:
                player = PlayerModel.objects.get(username__exact=player_username)
                table = PokerTableModel.objects.get(name__exact=player_table)
                if player in table.players.all():
                    if bet_chips < 0:
                        return "Error: can't bet negative amount"
                    # TODO
                    # is this a good idea?
                    if player.folded:
                        return "Cannot bet, player folded"
                    if (player.chips >= bet_chips and bet_chips > 0):
                        player.chips = player.chips - bet_chips
                        player.betAmount = bet_chips
                        table.pot += bet_chips
                        table.currentBet = bet_chips
                        table.save()
                        player.save()
                        floppedString = PokerGame.check_for_flop(self, player_table)
                        if 'nope' in floppedString:
                            return player_username + " successfully bet " + str(bet_chips) + " chips"
                        else:
                            return player_username + " successfully bet " + str(bet_chips) + " chips " + floppedString
                    else:
                        return "Error: bad bet chips value"
                else:
                    return "Player" + player_username + " is not a part of table " + table.name

        else:
            return "Error: need player username and table"


    def raisebet(self, bet_chips, player_username, player_table):
        if (player_username and player_table and bet_chips):
            if (PlayerModel.objects.filter(username__exact=player_username) == None):
                return "Error: " + player_username + " does not exist"
            elif(PokerTableModel.objects.filter(name__exact=player_table) == None):
                return "Error: " + player_table + " does not exist"
            else:
                player = PlayerModel.objects.get(username__exact=player_username)
                table = PokerTableModel.objects.get(name__exact=player_table)
                if player in table.players.all():
                    if bet_chips < 0:
                        return "Error: can't bet negative amount"
                    if player.folded:
                        return "Cannot bet, player folded"
                    if (player.chips >= bet_chips + table.currentBet):
                        player.chips = player.chips - (bet_chips + table.currentBet)
                        table.currentBet += bet_chips
                        player.betAmount = table.currentBet
                        table.pot += table.currentBet
                        table.save()
                        player.save()
                        floppedString = PokerGame.check_for_flop(self, player_table)
                        if 'nope' in floppedString:
                            return "Player " + player_username + " raised bet to " + str(table.currentBet)
                        else:
                            return "Player " + player_username + " raised bet to " + str(table.currentBet) + " " + floppedString

                    else:
                        return "Error: not enough chips"
                else:
                    return "Player" + player_username + " is not a part of table " + table.name
        else:
            return "Error: need player username table and chips"


    def call(self, player_username, player_table):
        if (player_username and player_table):
            if (PlayerModel.objects.filter(username__exact=player_username) == None):
                return "Error: " + player_username + " does not exist"
            elif(PokerTableModel.objects.filter(name__exact=player_table) == None):
                return "Error: " + player_table + " does not exist"
            else:
                player = PlayerModel.objects.get(username__exact=player_username)
                table = PokerTableModel.objects.get(name__exact=player_table)
                if player in table.players.all():
                    if (table.currentBet == 0):
                        return "Error: current bet is 0 can not call on a 0 bet"
                    elif (table.currentBet >= player.chips):
                        return "Error: player must go allin or fold"
                    if player.folded:
                        return "Cannot call, player folded"
                    else:
                        player.chips = player.chips - table.currentBet
                        player.betAmount = table.currentBet
                        table.pot += table.currentBet
                        player.save()
                        table.save()
                        floppedString = PokerGame.check_for_flop(self, player_table)
                        if 'nope' in floppedString:
                            return player_username + " called"
                        return player_username + " called " + floppedString
                else:
                    return "Player" + player_username + " is not a part of table " + table.name
        else:
            return "Error: need player username table and chips"




    def allin(self, player_username, player_table):
        if (player_username and player_table):
            if (PlayerModel.objects.filter(username__exact=player_username) == None):
                return "Error: " + player_username + " does not exist"
            elif(PokerTableModel.objects.filter(name__exact=player_table) == None):
                return "Error: " + player_table + " does not exist"
            else:
                player = PlayerModel.objects.get(username__exact=player_username)
                table = PokerTableModel.objects.get(name__exact=player_table)
                if player.folded:
                    return "Cannot bet, player folded"
                if player in table.players.all():
                    table.pot += player.chips
                    table.currentBet = player.chips
                    player.betAmount = player.chips
                    player.chips = 0
                    player.save()
                    table.save()
                    floppedString = PokerGame.check_for_flop(self, player_table)
                    if 'nope' in floppedString:
                        return "Player " + player_username + " went all in"
                    else:
                        return "Player " + player_username + " went all in " + floppedString
                else:
                    return "Player" + player_username + " is not a part of table " + table.name

        else:
            return "Error: need player username table and chips"

        #TODO write unit tests for quit game
    def quit_game(self, tablename):
        try:
            table = PokerTableModel.objects.get(name__exact=tablename)
            print(tablename)
        except PokerTableModel.DoesNotExist:
            return 'cannot quit because '+tablename+' does not exist'
        #check for existing player
        try:
            player = PokerTableModel.objects.get(players__username__exact=self.username, name__exact=tablename).players.get(username__exact=self.username)
        except PlayerModel.DoesNotExist:
            player = None
        if player:
            table.pot += player.chips
            player.folded = True
            table.players.remove(player)

    def join_game(self, tablename):
        #table doesn't exist
        try:
            table = PokerTableModel.objects.get(name__exact=tablename)
        except PokerTableModel.DoesNotExist:
            return 'cannot join because '+tablename+' does not exist'

        #check for existing player
        try:
            table_with_player = PokerTableModel.objects.get(players__username__exact=self.username, name__exact=tablename)
        except PokerTableModel.DoesNotExist:
            table_with_player = None
        if table_with_player:
            return self.username + " is already in " + tablename

        #table is full
        if (table.players.count() >=4):
            return tablename + " cannot add more players"

        table = PokerTableModel.objects.get(name=tablename)
        player = PlayerModel.objects.get(username__exact=self.username)
        player.hand.clear()
        player.folded = True
        player.save()
        table.players.add(player)
        table.save()



        #try to start a game if table gameStarted == false
        if(table.gameStarted == False):
            PokerGame.start_game(self, tablename)
        return self.username + " successfully joined " + table.name

    def fold(self, player_username, player_table):
        if (player_username and player_table):
            if (PlayerModel.objects.filter(username__exact=player_username) == None):
                return "Error: " + player_username + " does not exist"
            elif(PokerTableModel.objects.filter(name__exact=player_table) == None):
                return "Error: " + player_table + " does not exist"
            else:
                player = PlayerModel.objects.get(username__exact=player_username)
                table = PokerTableModel.objects.get(name__exact=player_table)
                if player in table.players.all():
                    if (player.folded == True):
                        return "Error: player already folded"
                    else:
                        player.folded = True
                        player.save()
                        floppedString = PokerGame.check_for_flop(self, player_table)
                        if 'nope' in floppedString:
                            return player_username + " folded"
                        else:
                            return player_username + " folded " + floppedString
                else:
                    return "Player" + player_username + " is not a part of table " + table.name
        else:
            return "Error: need player username table and chips"
