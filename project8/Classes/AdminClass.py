from project8.Classes.Player import Player
from project8.models import AdminModel, PlayerModel, \
    PokerTableModel, DealerModel, DeckModel, CardModel


class Admins(Player):

    def __init__(self, username, isonline):
        # super.__init__(self)
        self.isonline = isonline
        self.username = username
        if self.username is None or self.isonline is None:
            raise NameError('Admin cannot be accessed')
        if not isinstance(self.username, str):
            raise TypeError('Incorrect data provided for username')
        if not isinstance(self.isonline, bool):
            raise TypeError('Incorrect data provided for accessing Admin')


    def make_admin_online(self, admin_username):
        online_admins = AdminModel.objects.filter(isonline=True)

        if len(online_admins) >= 1:
            return "Error: " + online_admins[0].username +" is already the admin"
        if admin_username:
            AdminModel.objects.filter(username__exact=admin_username).update(isonline=True)
            return "Admin \"" + admin_username + "\" logged in successfully"
        else:
            return "Error: Cannot login. No username provided"


    def create_player(self, username):

        # check if player exists
        try:
            existing_player = PlayerModel.objects.get(username__exact=username)
        except PlayerModel.DoesNotExist:
            existing_player = None

        if existing_player:
            # check if player is already online
            if existing_player.isonline:
                return 'Cannot create ' + username + ' because ' + username + ' already exists'
            elif not existing_player.isonline:
            #make offline player -> online
                PlayerModel.objects.filter(username__exact=username).update(isonline=True)
                existing_player = PlayerModel.objects.get(username__exact=username)
                return "Created player: " + existing_player.username

        #create new player
        newplayer = PlayerModel.objects.create(username=username, isonline=True)
        return "Created player: " + newplayer.username


    def create_dealer(self, username):
        cards = []
        for s in range(1, 5):
            for r in range(1, 14):
                newcard = CardModel.objects.create(suit=s, rank=r)
                cards.append(newcard)
        newdeck = DeckModel.objects.create(name="deck1")
        for card in cards:
            newdeck.cards.add(card)
        newdeck.save()


        #check if already an online dealer with that username
        try:
            existing_dealer = DealerModel.objects.get(username__exact=username)
        except DealerModel.DoesNotExist:
            existing_dealer = None

        if existing_dealer:
            if existing_dealer.isonline:
                return 'Cannot create ' + username + ' because ' + username + ' already exists'
            elif not existing_dealer.isonline:
                # make offline dealer online
                DealerModel.objects.filter(username__exact=username).update(isonline=True)
                existing_dealer = DealerModel.objects.get(username__exact=username)
                return "Created dealer: " + existing_dealer.username

        #make a new dealer
        new_dealer = DealerModel.objects.create(username=username, isonline=True, deck=newdeck)
        return "Created dealer: " + new_dealer.username

    def create_table(self, tablename):

        #check if table exists
        try:
            existing_table = PokerTableModel.objects.get(name__exact=tablename)
        except PokerTableModel.DoesNotExist:
            existing_table = None

        if existing_table:
            return 'Cannot create ' + tablename + ' because ' + tablename + ' already exists'

        try:
            dealer = DealerModel.objects.get(isonline=True)
        #multiple online dealers -> assign to first dealer without a table
        except DealerModel.MultipleObjectsReturned:
            online_dealers = DealerModel.objects.filter(isonline=True)
            dealer_list = list(online_dealers)
            for dealername in dealer_list:
                try:
                    table = PokerTableModel.objects.get(dealer=dealername)
                except PokerTableModel.DoesNotExist:
                    newTable = PokerTableModel.objects.create(name=tablename, dealer=dealername)
                    return newTable.name + " created & assigned to Dealer \"" + dealername.username + "\""
            # multiple online dealers but already have tables
            return "please create an online dealer"
        except DealerModel.DoesNotExist:
            return "please create an online dealer"

        #one online dealer
        try:
            table = PokerTableModel.objects.get(dealer=dealer)
        except PokerTableModel.DoesNotExist:
            newTable = PokerTableModel.objects.create(name=tablename, dealer=dealer)
            return newTable.name + " created & assigned to Dealer \"" + dealer.username + "\""
        #one online dealer but already has table
        return "please create an online dealer"


    def reset_score(self, playername):
        try:
            player = PlayerModel.objects.get(username__exact=playername)
        except PlayerModel.DoesNotExist:
            return "player doesn't exist"
        if not player.isonline:
            return "player doesn't exist"

        player =  PlayerModel.objects.filter(username__exact=playername).update(highscore = 0)
        return playername + " high score reset to 0"