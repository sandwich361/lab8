#Call this to get the hand types for the game

class HandTypes:
    def __init__(self):
        pass

    def checkhandtype(self, cards):
        if HandTypes.isroyalflush(self, cards):
            return 10
        elif HandTypes.isstraightflush(self, cards):
            return 9
        elif HandTypes.isfourofakind(self, cards):
            return 8
        elif HandTypes.isfullhouse(self, cards):
            return 7
        elif HandTypes.isflush(self, cards):
            return 6
        elif HandTypes.isstraight(self, cards):
            return 5
        elif HandTypes.isthreeofakind(self, cards):
            return 4
        elif HandTypes.istwopair(self, cards):
            return 3
        elif HandTypes.ispair(self, cards):
            return 2
        else:
            return 1

    def isroyalflush(self, cards):
        cardlist = []
        i = 0
        for card in cards.all():
            cardlist.append(card)
            ++i
        for j in range(len(cardlist)):
            b = False
            i = 0
            while i < len(cardlist) - 1:
                if cardlist[i].rank < cardlist[i + 1].rank:
                    swap = cardlist[i]
                    cardlist[i] = cardlist[i + 1]
                    cardlist[i + 1] = swap
                    b = True
                i = i + 1
            if b == False:
                break
        card1 = cardlist[0]
        card2 = cardlist[1]
        card3 = cardlist[2]
        card4 = cardlist[3]
        card5 = cardlist[4]
        if card1.suit == card2.suit & card2.suit == card3.suit & card3.suit == card4.suit & card4.suit == card5.suit:
            if card1.rank == 10 & card2.rank == 11 & card3.rank == 12 & card4.rank == 13 & card5.rank == 1:
                return True
        return False

    def isstraightflush(self, cards):
        cardlist = []
        i = 0
        for card in cards.all():
            cardlist.append(card)
            ++i
        for j in range(len(cardlist)):
            b = False
            i = 0
            while i < len(cardlist) - 1:
                if cardlist[i].rank < cardlist[i + 1].rank:
                    swap = cardlist[i]
                    cardlist[i] = cardlist[i + 1]
                    cardlist[i + 1] = swap
                    b = True
                i = i + 1
            if b == False:
                break
        card1 = cardlist[0]
        card2 = cardlist[1]
        card3 = cardlist[2]
        card4 = cardlist[3]
        card5 = cardlist[4]
        if card1.suit == card2.suit & card2.suit == card3.suit & card3.suit == card4.suit & card4.suit == card5.suit:
            if card1.rank == card2.rank - 1 & card2.rank == card3.rank - 1 & card3.rank == card4.rank - 1 & card4.rank == card5.rank - 1:
                return True
        return False

    def isfourofakind(self, cards):
        cardlist = []
        i = 0
        for card in cards.all():
            cardlist.append(card)
            ++i
        for j in range(len(cardlist)):
            b = False
            i = 0
            while i < len(cardlist) - 1:
                if cardlist[i].rank < cardlist[i + 1].rank:
                    swap = cardlist[i]
                    cardlist[i] = cardlist[i + 1]
                    cardlist[i + 1] = swap
                    b = True
                i = i + 1
            if b == False:
                break
        card1 = cardlist[0]
        card2 = cardlist[1]
        card3 = cardlist[2]
        card4 = cardlist[3]
        card5 = cardlist[4]
        if card1.rank == card2.rank & card2.rank == card3.rank & card3.rank == card4.rank:
            return True
        elif card2.rank == card3.rank & card3.rank == card4.rank & card4.rank == card5.rank:
            return True
        return False

    def isfullhouse(self, cards):
        cardlist = []
        i = 0
        for card in cards.all():
            cardlist.append(card)
            ++i
        for j in range(len(cardlist)):
            b = False
            i = 0
            while i < len(cardlist) - 1:
                if cardlist[i].rank < cardlist[i + 1].rank:
                    swap = cardlist[i]
                    cardlist[i] = cardlist[i + 1]
                    cardlist[i + 1] = swap
                    b = True
                i = i + 1
            if b == False:
                break
        card1 = cardlist[0]
        card2 = cardlist[1]
        card3 = cardlist[2]
        card4 = cardlist[3]
        card5 = cardlist[4]
        if card1.rank == card2.rank & card2.rank == card3.rank & card4.rank == card5.rank:
            return True
        elif card1.rank == card2.rank & card3.rank == card4.rank & card4.rank == card5.rank:
            return True
        return False

    def isflush(self, cards):
        cardlist = []
        i = 0
        for card in cards.all():
            cardlist.append(card)
            ++i
        for j in range(len(cardlist)):
            b = False
            i = 0
            while i < len(cardlist) - 1:
                if cardlist[i].rank < cardlist[i + 1].rank:
                    swap = cardlist[i]
                    cardlist[i] = cardlist[i + 1]
                    cardlist[i + 1] = swap
                    b = True
                i = i + 1
            if b == False:
                break
        card1 = cardlist[0]
        card2 = cardlist[1]
        card3 = cardlist[2]
        card4 = cardlist[3]
        card5 = cardlist[4]
        if card1.suit == card2.suit & card2.suit == card3.suit & card3.suit == card4.suit & card4.suit == card5.suit:
            return True
        return False

    def isstraight(self, cards):
        cardlist = []
        i = 0
        for card in cards.all():
            cardlist.append(card)
            ++i
        for j in range(len(cardlist)):
            b = False
            i = 0
            while i < len(cardlist) - 1:
                if cardlist[i].rank < cardlist[i + 1].rank:
                    swap = cardlist[i]
                    cardlist[i] = cardlist[i + 1]
                    cardlist[i + 1] = swap
                    b = True
                i = i + 1
            if b == False:
                break
        card1 = cardlist[0]
        card2 = cardlist[1]
        card3 = cardlist[2]
        card4 = cardlist[3]
        card5 = cardlist[4]
        if card1.rank == card2.rank - 1 & card2.rank == card3.rank - 1 & card3.rank == card4.rank - 1 & card4.rank == card5.rank - 1:
            return True
        return False

    def isthreeofakind(self, cards):
        cardlist = []
        i = 0
        for card in cards.all():
            cardlist.append(card)
            ++i
        for j in range(len(cardlist)):
            b = False
            i = 0
            while i < len(cardlist) - 1:
                if cardlist[i].rank < cardlist[i + 1].rank:
                    swap = cardlist[i]
                    cardlist[i] = cardlist[i + 1]
                    cardlist[i + 1] = swap
                    b = True
                i = i + 1
            if b == False:
                break
        lag1 = None
        lag2 = None
        for card in cardlist:
            if lag1 == None:
                lag1 = card
            else:
                lag2 = lag1
                lag1 = card
                if card.rank == lag1.rank & card.rank == lag2.rank:
                    return True
        return False

    def istwopair(self, cards):
        cardlist = []
        i = 0
        for card in cards.all():
            cardlist.append(card)
            ++i
        for j in range(len(cardlist)):
            b = False
            i = 0
            while i < len(cardlist) - 1:
                if cardlist[i].rank < cardlist[i + 1].rank:
                    swap = cardlist[i]
                    cardlist[i] = cardlist[i + 1]
                    cardlist[i + 1] = swap
                    b = True
                i = i + 1
            if b == False:
                break
        card1 = cardlist[0]
        card2 = cardlist[1]
        card3 = cardlist[2]
        card4 = cardlist[3]
        card5 = cardlist[4]
        if card1.rank == card2.rank & card3.rank == card4.rank:
            return True
        elif card1.rank == card2.rank & card4.rank == card5.rank:
            return True
        elif card2.rank == card3.rank & card4.rank == card5.rank:
            return True
        return False


    def ispair(self, cards):
        cardlist = []
        i = 0
        for card in cards.all():
            cardlist.append(card)
            ++i
        for j in range(len(cardlist)):
            b = False
            i = 0
            while i < len(cardlist) - 1:
                if cardlist[i].rank < cardlist[i + 1].rank:
                    swap = cardlist[i]
                    cardlist[i] = cardlist[i + 1]
                    cardlist[i + 1] = swap
                    b = True
                i = i + 1
            if b == False:
                break
        lag = None
        for card in cardlist:
            if lag == None:
                lag = card
            else:
                if card.rank == lag.rank:
                    return True
            lag = card
        return False

