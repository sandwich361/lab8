from project8.Classes.Card import Card
import random

class deck:
  # added - Noah Ratzburg - 2/26/20
  # deck will be a dictionary mapping cards to boolean values
  # cards will be a list of cards treated as a stack

  def __init__(self):
    #create a standard 52 card deck of cards
    self.cardList = []
    for s in range(1, 5):
      for r in range(1, 14):
        self.cardList.append(Card(s,r))
    self.top = 52

  def count(self):
    #return the number of cards remaining in the deck
    return self.top

  def draw(self):
    #return and remove the top card in the deck
    #if the deck is empty, raise a ValueErro
    if self.top == 0:
      raise ValueError('no more cards')
    card = self.cardList[self.top-1]
    self.top = self.top - 1

    if not isinstance(card, Card):
      raise TypeError('draw should only return a Card')

    return card


  def shuffle(self):
    #shuffle the deck using a random number generator
    random.shuffle(self.cardList)


  def getSize(self):
    # size of deck
    return len(self.cardList)

