from project8.Classes.Dealer import Dealer
from project8.models import PlayerModel
from project8.models import PokerTableModel, PlayerModel, DealerModel


class PokerGame:
    def __init__(self, table, dealer, players):
        self.table = table
        self.pot = 0
        self.dealer = dealer
        self.players = players
        self.currentBet = 0
        self.activePlayers = players
        self.foldedPlayers = None
        self.playerBets = {}
        self.status = ''

    def start_game(self, tablename):
        table = PokerTableModel.objects.get(name=tablename)
        if(table.players.count() >= 2):
            game = PokerGame.__init__(self, tablename, table.dealer, table.players)
            PokerGame.play_poker(self)
        else:
            self.status = 'waiting for more players'
            return self.status

    def play_poker(self):
        # starts the poker game
        dealer = DealerModel.objects.get(username=self.dealer)
        table_name = self.table
        dealer_instance = Dealer(dealer.username)
        dealer_instance.deal_cards(table_name, dealer.deck)
        table = PokerTableModel.objects.get(name=self.table)
        for player in table.players.all():
            player = PlayerModel.objects.get(username=player)
            player.folded = False
            player.save()
        table.gameStarted = True
        table.save()

    def check_for_flop(self, table):
        # call dealer flop once the first player bet and the bet amounts of all players are all the same
        table = PokerTableModel.objects.get(name=table)
        neededAmount = PokerTableModel.objects.get(name=table).currentBet
        if (neededAmount != 0):
            onlyOneNonFolded = False
            countOfNonFolded = 0
            for player in table.players.all():
                if PlayerModel.objects.get(username=player).folded == False:
                        countOfNonFolded = countOfNonFolded + 1
            if countOfNonFolded == 1:
                onlyOneNonFolded = True
            equalBets = True
            for player in table.players.all():
                if (PlayerModel.objects.get(username=player).folded != True):
                    if PlayerModel.objects.get(username=player).betAmount != neededAmount:
                        equalBets = False
            if ((equalBets == True) or (onlyOneNonFolded == True)):
                dealer = DealerModel.objects.get(username=PokerTableModel.objects.get(name=table.name).dealer)
                table_name = table.name
                flopString = Dealer.flop(self, dealer.username, table_name)
                return flopString
        return "nope"