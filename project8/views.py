from django.http import request
from django.shortcuts import render, redirect
from django.views import View

from project8.Classes.Player import Player
from project8.models import DealerModel, PlayerModel, PokerTableModel, User, AdminModel
from project8.Classes.Command import Command
from project8.Classes.AdminClass import Admins

# Create your views here.

class Home(View):
    message = ""

    def get(self, request):
        return render(request, 'main/index.html',
                      {
                          'dealers': DealerModel.objects.filter(isonline=True),
                          'players': PlayerModel.objects.all(),
                          'tables': PokerTableModel.objects.all(),

                      })

    def post(self, request):
        ui = Command()
        user_input = request.POST["username_input"]
        user_command = request.POST["command_input"]

        print("username: " + user_input + "\n command:" + user_command)
        full_prompt = user_input + ":" + user_command

        if full_prompt:
            message = ui.check_command(full_prompt)
        else:
            message = "no command provided"

        allcommands = ui.allcommands

        return render(request, 'main/index.html',
                      {
                          'message': message,
                          'dealers': DealerModel.objects.filter(isonline=True),
                          'players': PlayerModel.objects.all(),
                          'tables': PokerTableModel.objects.all(),
                          'allcommands': allcommands
                      })


class LoginView(View):
    def get(self, request):
        if request.session.get("username"):
            return redirect("alltables")
        return render(request, "main/login.html")

    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]
        user = User.objects.all().filter(username=username)

        if user.count() == 0 or user[0].password != password:
            return render(request, "main/login.html", {"error_messages": "username/password incorrect"})
        request.session["username"] = username
        return redirect("alltables")


class RegisterView(View):
    def get(self, request):
        return render(request, "main/register.html")

    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]

        # check no other user has the same name
        check_user = User.objects.all().filter(username=username)
        if check_user.count() != 0:
            error_messages = f"{username} exists, please use another username"
            return render(request, "main/register.html", {"error_messages": error_messages})

        User.objects.create(username=username, password=password)
        PlayerModel.objects.create(username=username, password=password, isonline=True)

        return redirect("login")


class Tables(View):
    # shows list of tables
    def get(self, request):
        if not request.session.get("username"):
            return redirect("login")
        username = request.session["username"]
        print(username)
        playername = str(username)
        try:
            player = PlayerModel.objects.all().filter(username__exact=playername)
            print(player)
        except PlayerModel.DoesNotExist:
            print("doesnt")

        players = PlayerModel.objects.all()
        tables = PokerTableModel.objects.all()

        return render(request, "main/tables.html",
                      {"username": username,
                       "players": players,
                       "tables": tables,
                       })

    def post(self, request):
        username = request.session["username"]
        try:
            admin = AdminModel.objects.get(username__exact=username)
        except AdminModel.DoesNotExist:
            admin = None
        if admin:
            if 'create_table' in request.POST:
                tablename = request.POST["tablename_create"]
                dealername = request.POST["dealer_create"]

                # check no other table has the same name
                check_table = PokerTableModel.objects.all().filter(name=tablename)
                if check_table.count() != 0:
                    error_messages = f"{tablename} exists, please create another table"
                    return render(request, "main/tables.html.html", {"error_messages": error_messages})

                check_dealer = DealerModel.objects.all().filter(username=dealername)
                if check_dealer.count() != 0:
                    error_messages = f"{dealername} exists, please create another dealer"
                    return render(request, "main/tables.html.html", {"error_messages": error_messages})

                Admins.create_dealer(self, dealername)
                Admins.create_table(self, tablename)
                return redirect("alltables")

            elif 'delete_table' in request.POST:
                table_delete = request.POST["tablename_delete"]
                PokerTableModel.objects.all().filter(name=table_delete).delete()
                return redirect("alltables")


class Users(View):

    # shows list of tables
    def get(self, request):
        if not request.session.get("username"):
            return redirect("login")

        username = request.session["username"]
        users = User.objects.all()
        try:
            admin = AdminModel.objects.get(username__exact=username)
        except AdminModel.DoesNotExist:
            admin = None
        if admin:
            return render(request, "main/users.html",
                          {"username": username,
                           "users": users,
                           })
        else:
            access_denied = f"{username} is not an admin, access denied"
            return render(request, "main/usersdenied.html", {"access_denied": access_denied})

    def post(self, request):
        if 'create_user' in request.POST:
            username = request.POST["username_create"]
            password = request.POST["password_create"]

            # check no other user has the same name
            check_user = User.objects.all().filter(username=username)
            if check_user.count() != 0:
                error_messages = f"{username} exists, please use another username"
                return render(request, "main/users.html", {"error_messages": error_messages})

            User.objects.create(username=username, password=password)
            PlayerModel.objects.create(username=username, password=password)
            return redirect("users")
        elif 'delete_user' in request.POST:
            user_delete = request.POST["user_delete"]
            User.objects.all().filter(username=user_delete).delete()
            PlayerModel.objects.all().filter(username=user_delete).delete()
            return redirect("users")


class Game(View):  # Table
    prompts = 'prompts'
    def get(self, request, tablename):
        if not request.session.get("username"):
            return redirect("login")

        username = request.session["username"]
        playername = str(username)
        try:
            player = PlayerModel.objects.all().get(username__exact=playername)
        except PlayerModel.DoesNotExist:
            return redirect("register")

        # display all players existing in table
        try:
            table = PokerTableModel.objects.get(name__exact=tablename)
        except PokerTableModel.DoesNotExist:
            return redirect("alltables")

        print(table.players.all())

        #if the player exists, make sure they are shown as existing
        # we can quickly change variables using a dictionary
        seats = dict.fromkeys(["takenseat1", "takenseat2", "takenseat3", "takenseat4"], False)
        players_dict = dict.fromkeys(["player1","player2","player3","player4"], None)
        i = 1
        for player_in_table in table.players.all():
            player_in_table_name = 'player' + str(i)
            players_dict[player_in_table_name] = player_in_table
            result = 'takenseat' + str(i)
            seats[result] = True
            i += i
        tableprompt = "Current bet: " + str(table.currentBet)

        #return view
        context = { "player": player, "username": username,
                    "taken1": seats.get("takenseat1"),
                    "player1": players_dict.get("player1"),
                    "taken2": seats.get("takenseat2"),
                    "player2": players_dict.get("player2"),
                    "taken3": seats.get("takenseat3"),
                    "player3": players_dict.get("player3"),
                    "taken4": seats.get("takenseat4"),
                    "player4": players_dict.get("player4"),
                    "prompt": tableprompt,
                    }


        return render(request, "main/table.html", context)

    def post(self, request, tablename):
        username = request.session["username"]
        playername = str(username)
        try:
            player = PlayerModel.objects.all().get(username__exact=playername)
        except PlayerModel.DoesNotExist:
            return redirect("register")


        player_instance = Player(username=playername)
        try:
            table = PokerTableModel.objects.get(name__exact=tablename)
        except PokerTableModel.DoesNotExist:
            return redirect("alltables")
        tableprompt = "Current bet: " + str(table.currentBet)

        #if the player exists, make sure they are shown as existing
        # we can quickly change variables using a dictionary
        takenseats = dict.fromkeys(["takenseat1", "takenseat2", "takenseat3", "takenseat4"], False)
        players_dict = dict.fromkeys(["player1","player2","player3","player4"], None)
        i = 1
        for player_in_table in table.players.all():
            player_in_table_name = 'player' + str(i)
            players_dict[player_in_table_name] = player_in_table
            result = 'takenseat' + str(i)
            takenseats[result] = True
            i += i

        if 'refresh' in request.POST:
            return redirect("table", tablename)

        # get value of radio inputs to call methods
        if 'go' in request.POST:
            #use request.POST.get("buttons") to return 'value' of buttons: fold, Call, Raise1
            if 'Fold' in request.POST.get("buttons"):
                self.prompts = player_instance.fold(playername,tablename)
            elif 'Call' in request.POST.get("buttons"):
                self.prompts = player_instance.call(playername, tablename)
            elif 'Raise1' in request.POST.get("buttons"):
                self.prompts = player_instance.bet(1, playername, tablename)
            elif 'Raise2' in request.POST.get("buttons"):
                self.prompts = player_instance.bet(2, playername, tablename)
            elif 'Raise3' in request.POST.get("buttons"):
                self.prompts = player_instance.bet(3, playername, tablename)
            elif 'Raise4' in request.POST.get("buttons"):
                self.prompts = player_instance.bet(4, playername, tablename)
            elif 'Raise5' in request.POST.get("buttons"):
                self.prompts = player_instance.bet(5, playername, tablename)
        elif 'quit' in request.POST:
            player_instance.quit_game(tablename)
            self.prompts = f"{playername} quit game"



        if player:

            if 'sit1' in request.POST:
                if player not in table.players.all():
                    takenseats["takenseat1"] = True
                    players_dict["player1"] = player
                    #make player join game
                    player_instance.join_game(tablename=table.name)
            elif 'sit2' in request.POST:
                if player not in table.players.all():
                    takenseats["takenseat2"] = True
                    players_dict["player2"] = player
                    player_instance.join_game(tablename=table.name)
            elif 'sit3' in request.POST:
                if player not in table.players.all():
                    takenseats["takenseat3"] = True
                    players_dict["player3"] = player
                    player_instance.join_game(tablename=table.name)
            elif 'sit4' in request.POST:
                if player not in table.players.all():
                    takenseats["takenseat4"] = True
                    players_dict["player4"] = player
                    player_instance.join_game(tablename=table.name)

            context = {"player": player, "username": username,
                       "taken1": takenseats.get("takenseat1"),
                       "player1": players_dict.get("player1"),
                       "taken2": takenseats.get("takenseat2"),
                       "player2": players_dict.get("player2"),
                       "taken3": takenseats.get("takenseat3"),
                       "player3": players_dict.get("player3"),
                       "taken4": takenseats.get("takenseat4"),
                       "player4": players_dict.get("player4"),
                        "prompt": self.prompts + " " +tableprompt}


            return render(request, "main/table.html", context)
