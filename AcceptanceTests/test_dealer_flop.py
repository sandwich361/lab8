from django.test import TestCase
from project8.models import PlayerModel, DealerModel, AdminModel, CardModel,DeckModel, PokerTableModel
from project8.Classes.Command import Command


class DealerTestDealSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)
        cards = []
        for s in range(1, 5):
            for r in range(1, 14):
                newcard = CardModel.objects.create(suit=s, rank=r)
                cards.append(newcard)
        newdeck = DeckModel.objects.create(name="deck1")
        for card in cards:
            newdeck.cards.add(card)
        newdeck.save()
        self.maindealer = DealerModel.objects.create(username="MainDealer", isonline=True, deck=newdeck)
        self.player1 = PlayerModel.objects.create(username="player1", isonline=True)
        self.player2 = PlayerModel.objects.create(username="player2", isonline=True)
        self.table = PokerTableModel.objects.create(name="table1", dealer=self.maindealer)

    def test_good_flop(self):
        table1 = PokerTableModel.objects.get(name__exact="table1")
        table1.players.add(self.player1)
        table1.players.add(self.player2)
        table1.save()
        self.assertEqual(self.ui.check_command("MainDealer:deal table1"), "MainDealer successfully dealed cards")
        self.assertEqual(self.ui.check_command("player1:bet 5 table1"), "player1 successfully bet 5 chips")
        self.assertEqual(self.ui.check_command("player2:bet 5 table1"), "player2 successfully bet 5 chips")
        self.assertEqual(self.ui.check_command("MainDealer: flop table1"), 'Error: a player must bet before the flop')

