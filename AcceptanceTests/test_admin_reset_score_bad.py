from django.test import TestCase
from project8.models import AdminModel, PlayerModel, DealerModel
from project8.Classes.Command import Command

# As an admin, I can reset the high score, so game restarts

# a. when admin calls "resetscore <playername>",
# the total score of the game at that player is reset to 0

#typos -> please type in correct command
#no player provided -> please choose a player
#multiple players -> please choose a player
#invalid player name/player doesn't exist -> player doesn't exist
# player is offline -> PlayerModel
class AdminTestCreateSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)
        maindealer = DealerModel.objects.create(username="MegaSandwich", isonline=True)
        PlayerModel.objects.create(username="player1", isonline=True)
        PlayerModel.objects.create(username="player2", isonline=False)

    def test_reset_score_typo(self):
        self.assertEqual(self.ui.check_command("AdminSandwich: reset score player1"),
                         "please type in correct command")
        self.assertEqual(self.ui.check_command("AdminSandwich: resetscores player1"),
                         "please type in correct command")

    def test_reset_score_no_player(self):
        self.assertEqual(self.ui.check_command("AdminSandwich: resetscore"),
                         "please choose a player")

    def test_reset_score_multiple_players(self):
        self.assertEqual(self.ui.check_command("AdminSandwich: resetscore player1 player2"),
                         "please choose a player")
        self.assertEqual(self.ui.check_command("AdminSandwich: resetscore player1 player1"),
                         "please choose a player")

    def test_reset_score_incorrect_playername(self):
        players = PlayerModel.objects.filter(isonline=True)

        self.assertEqual(self.ui.check_command("AdminSandwich: resetscore player2"),
                         "player doesn't exist")
        self.assertEqual(self.ui.check_command("AdminSandwich: resetscore player3"),
                         "player doesn't exist")

