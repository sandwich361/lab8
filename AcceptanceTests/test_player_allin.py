from django.test import TestCase
from project8.models import DealerModel
from project8.models import PlayerModel
from project8.models import PokerTableModel
from project8.Classes.Command import Command

# •	As a player I can call ‘allin’ command so that I can bet all of my chips
# o	When a player calls “allin” <tablename> their chip count will be 0.
# o	If the player is not a part of that table, an error message will appear saying they are not a part of that table
# o	If the table does not exist, tan error message will appear specifying that

class TestPlayerAllIn(TestCase):

    def setUp(self):
        self.ui = Command()

        self.player1 = PlayerModel.objects.create(username="player1", isonline=True, chips=75)
        self.player2 = PlayerModel.objects.create(username="player2", isonline=True, chips=50)
        self.player3 = PlayerModel.objects.create(username="player3", isonline=True, chips=100)
        self.player4 = PlayerModel.objects.create(username="player4", isonline=True, chips=15)
        self.player5 = PlayerModel.objects.create(username="player5", isonline=False, chips=0)

        self.dealer = DealerModel.objects.create(username="dealer1", isonline=True)
        self.table1 = PokerTableModel.objects.create(name="table1", dealer=self.dealer)
        self.table2 = PokerTableModel.objects.create(name="table2", dealer=self.dealer)

        list_1 = [self.player1, self.player2]
        list_2 = [self.player3, self.player4]

        for player in list_1:
            self.table1.players.add(player)
        self.table1.save()
        for player in list_2:
            self.table2.players.add(player)
        self.table2.save()

    def test_allin_good(self):
        self.assertEqual(self.ui.check_command("player1:allin table1"), "Player player1 went all in")
        self.assertEqual(self.ui.check_command("player4:allin table2"), "Player player4 went all in")

    def test_nonexistent_player_allin(self):
        self.assertEqual(self.ui.check_command("player7:allin table1"), "can't go all in because player does not exist")
        self.assertEqual(self.ui.check_command("babysandwich:allin table2"), "can't go all in because player does not exist")

    def test_allin_not_enough_args(self):
        self.assertEqual(self.ui.check_command("player3:allin"), "command must only include 'allin' <name of table>")
        self.assertEqual(self.ui.check_command("player2:allin"), "command must only include 'allin' <name of table>")

    def test_allin_too_many_args(self):
        self.assertEqual(self.ui.check_command("player3:allin table2 now"), "command must only include 'allin' <name of table>")
        self.assertEqual(self.ui.check_command("player3:allin in table2"), "command must only include 'allin' <name of table>")
        self.assertEqual(self.ui.check_command("player2:allin table2 5 7"), "command must only include 'allin' <name of table>")

    def test_allin_written_incorrect(self):
        self.assertEqual(self.ui.check_command("player4:alllin table1 "), 'please type in correct command')
        self.assertEqual(self.ui.check_command("player2:allintable1"), "allin command written incorrectly")

    def test_allin_nonexistent_table(self):
        self.assertEqual(self.ui.check_command("player1:allin table3"), "Error: table3 does not exist")
        self.assertEqual(self.ui.check_command("player2:allin tablee"), "Error: tablee does not exist")
        self.assertEqual(self.ui.check_command("player4:allin bigtable"), "Error: bigtable does not exist")

    def test_allin_player_not_in_table(self):
        self.assertEqual(self.ui.check_command("player1:allin table2"), "Player player1 is not a part of table table2")
        self.assertEqual(self.ui.check_command("player4:allin table1"), "Player player4 is not a part of table table1")
