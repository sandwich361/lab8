from django.test import TestCase
from project8.models import AdminModel, PokerTableModel, DealerModel
from project8.Classes.Command import Command

# As an admin, I can reset the high score, so game restarts


# b. if there are no online dealers & admin calls
# "createtable <tablename>" , a response to create
#  an online dealer comes first


# no online dealers -> please create an online dealer
# online dealer but table is already assigned to them -> please create an online dealer
# typos -> please type in correct command
# no tablename provided -> please type in 1 table name
# multiple tables -> please type in 1 table name
# table already exists -> "Cannot create tablename because tablename already exists"

class AdminTestCreateSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)
        DealerModel.objects.create(username="MegaSandwich", isonline=False)

    # no online dealers -> please create an online dealer
    def test_create_table_no_online_dealers(self):
        online_dealers = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealers), 0)
        self.assertEqual(self.ui.check_command("AdminSandwich: createtable Table1"),
                         "please create an online dealer")

    # online dealer but table is already assigned to them -> please create an online dealer
    def test_create_table_no_new_dealers(self):
        DealerModel.objects.filter(username="MegaSandwich").update(isonline=True)
        online_dealers = DealerModel.objects.filter(isonline=True)
        self.assertEqual( online_dealers.count(), 1)
        self.assertEqual(self.ui.check_command("AdminSandwich: createtable Table1"),
                         "Table1 created & assigned to Dealer \"MegaSandwich\"")
        table1 = PokerTableModel.objects.get(name__exact="Table1")
        maindealer = DealerModel.objects.get(username="MegaSandwich")
        self.assertEqual(table1.dealer.username, maindealer.username)

        #trying to create another table
        self.assertEqual(self.ui.check_command("AdminSandwich: createtable Table2"),
                         "please create an online dealer")

    # typos -> please type in correct command
    def test_create_table_typos(self):
        self.assertEqual(self.ui.check_command("AdminSandwich: createtableTable1"),
                         "please type in correct command")
        self.assertEqual(self.ui.check_command("AdminSandwich: create table Table1"),
                         "please type in correct command")

    # no tablename provided -> please type in 1 table name
    def test_create_table_no_table(self):
        self.assertEqual(self.ui.check_command("AdminSandwich: createtable"),
                         "Please type in 1 table name")


    # multiple tables -> please type in 1 table name
    def test_create_table_multiple_tables(self):
        self.assertEqual(self.ui.check_command("AdminSandwich: createtable Table1 Table2"),
                         "Please type in 1 table name")

    def test_create_table_table_exists(self):
        maindealer = DealerModel.objects.filter(username="MegaSandwich").update(isonline=True)
        online_dealers = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealers), 1)
        self.assertEqual(self.ui.check_command("AdminSandwich: createtable Table1"),
                         "Table1 created & assigned to Dealer \"MegaSandwich\"")
        active_tables = PokerTableModel.objects.all()
        self.assertEqual(len(active_tables), 1)
        self.assertEqual(self.ui.check_command("AdminSandwich: createtable Table1"),
                         "Cannot create Table1 because Table1 already exists")




