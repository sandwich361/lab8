from django.test import TestCase
from project8.models import DealerModel
from project8.models import PlayerModel
from project8.models import PokerTableModel
from project8.Classes.Command import Command
# •	As a player I can call a ‘raisebet’ command so that the current bet of the table increases
# o	When a player calls “raisebet” <amount> <tablename> the current bet of the table will increase by the given amount
# o	The player’s chips will also decrease by the amount given + the table’s current bet
# o	If the amount entered is more than the (players chips + table’s current bet), an error message will appear specifying that the player doesn’t have enough chips
# o	If the player is not a part of that table, an error message will appear saying they are not a part of that table
# o	If the table does not exist, an error message will appear specifying that
# o	If the raise bet amount is not an integer, an error message will appear and the player will not be able to raise bet
# o	If the raise bet amount is negative, an error message will appear, and the player will not be able to raise bet


class TestPlayerRaiseBet(TestCase):

    def setUp(self):
        self.ui = Command()

        self.player1 = PlayerModel.objects.create(username="player1", isonline=True, chips=75)
        self.player2 = PlayerModel.objects.create(username="player2", isonline=True, chips=100)
        self.player3 = PlayerModel.objects.create(username="player3", isonline=True, chips=87)
        self.player4 = PlayerModel.objects.create(username="player4", isonline=True, chips=20)
        self.player5 = PlayerModel.objects.create(username="player5", isonline=False)

        self.dealer = DealerModel.objects.create(username="dealer1", isonline=True)
        self.table1 = PokerTableModel.objects.create(name="table1", dealer=self.dealer)
        self.table2 = PokerTableModel.objects.create(name="table2", dealer=self.dealer)

        list_1 = [self.player1, self.player2]
        list_2 = [self.player3, self.player4]

        for player in list_1:
            self.table1.players.add(player)
        self.table1.save()
        for player in list_2:
            self.table2.players.add(player)
        self.table2.save()

    def test_raise_good(self):
        self.table1.currentBet = 10
        self.table1.save()
        self.table2.currentBet = 15
        self.table2.save()
        self.assertEqual(self.ui.check_command("player1:raisebet 7 table1"), "Player player1 raised bet to 17")
        self.assertEqual(self.ui.check_command("player4:raisebet 5 table2"), "Player player4 raised bet to 20")

    def test_nonexistent_player_raise(self):
        self.assertEqual(self.ui.check_command("player6:raisebet 20 table1"), "can't raise bet because player does not exist")
        self.assertEqual(self.ui.check_command("babysandwich:raisebet 30 table2"), "can't raise bet because player does not exist")

    def test_nonInteger_raise(self):
        self.assertEqual(self.ui.check_command("player1:raisebet xyz table1"), 'raise bet amount not an integer')
        self.assertEqual(self.ui.check_command("player1:raisebet 7.5 table1"), 'raise bet amount not an integer')
        self.assertEqual(self.ui.check_command("player2:raisebet {1} table1"), 'raise bet amount not an integer')

    def test_raise_not_enough_args(self):
        self.assertEqual(self.ui.check_command("player2:raisebet 5"), "command must only include 'raisebet' <amount> <name of table>")
        self.assertEqual(self.ui.check_command("player1:raisebet"), "command must only include 'raisebet' <amount> <name of table>")
        self.assertEqual(self.ui.check_command("player4:raisebet table2"), "command must only include 'raisebet' <amount> <name of table>")

    def test_raise_too_many_args(self):
        self.assertEqual(self.ui.check_command("player4:raisebet 5 table2 right now"), "command must only include 'raisebet' <amount> <name of table>")
        self.assertEqual(self.ui.check_command("player4:raisebet 71 chips allin"), "command must only include 'raisebet' <amount> <name of table>")
        self.assertEqual(self.ui.check_command("player2:raisebet 67 109 23 table2"), "command must only include 'raisebet' <amount> <name of table>")

    def test_raise_written_incorrect(self):
        self.assertEqual(self.ui.check_command("player4: raisebet34 "), "raisebet command written incorrectly")
        self.assertEqual(self.ui.check_command("player2:rraisebet 67 "), "raisebet command written incorrectly")

    def test_raise_not_enough_chips(self):
        self.table1.currentBet = 85
        self.table1.save()
        self.table2.currentBet = 70
        self.table2.save()
        self.assertEqual(self.ui.check_command("player4:raisebet 10 table2"), "Error: not enough chips")
        self.assertEqual(self.ui.check_command("player1:raisebet 16 table1"), "Error: not enough chips")
        self.assertEqual(self.ui.check_command("player3:raisebet 20 table2"), "Error: not enough chips")

    def test_raise_negative_bet(self):
        self.assertEqual(self.ui.check_command("player1:raisebet -15 table1"), "Error: can't bet negative amount")
        self.assertEqual(self.ui.check_command("player3:raisebet -50 table2"), "Error: can't bet negative amount")

    def test_raise_nonexistent_table(self):
        self.assertEqual(self.ui.check_command("player1:raisebet 5 table3"), "Error: table3 does not exist")
        self.assertEqual(self.ui.check_command("player2:raisebet 5 tablee"), "Error: tablee does not exist")
        self.assertEqual(self.ui.check_command("player4:raisebet 10 bigtable"), "Error: bigtable does not exist")

    def test_raise_player_not_in_table(self):
        self.assertEqual(self.ui.check_command("player1:raisebet 10 table2"), "Player player1 is not a part of table table2")
        self.assertEqual(self.ui.check_command("player4:raisebet 13 table1"), "Player player4 is not a part of table table1")

