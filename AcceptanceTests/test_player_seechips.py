from django.test import TestCase
from project8.models import DealerModel
from project8.models import PlayerModel
from project8.models import PokerTableModel
from project8.Classes.Command import Command

# •	As a player I can call “seechips” so that I can see the total amount of chips I have
# o	When a player calls “seechips” table name, a message will appear showing them their chip count. It is also always displayed on the table.
# o	If the player is not a part of that table, an error message will appear saying they are not a part of that table
# o	If the table does not exist, an error message will appear specifying that


class TestPlayerSeeChips(TestCase):

    def setUp(self):
        self.ui = Command()

        self.player1 = PlayerModel.objects.create(username="player1", isonline=True, chips=75)
        self.player2 = PlayerModel.objects.create(username="player2", isonline=True, chips=100)
        self.player3 = PlayerModel.objects.create(username="player3", isonline=True, chips=100)
        self.player4 = PlayerModel.objects.create(username="player4", isonline=True, chips=5)
        self.player5 = PlayerModel.objects.create(username="player5", isonline=False)

        self.dealer = DealerModel.objects.create(username="dealer1", isonline=True)
        self.table1 = PokerTableModel.objects.create(name="table1", dealer=self.dealer)
        self.table2 = PokerTableModel.objects.create(name="table2", dealer=self.dealer)

        list_1 = [self.player1, self.player2]
        list_2 = [self.player3, self.player4]

        for player in list_1:
            self.table1.players.add(player)
        self.table1.save()
        for player in list_2:
            self.table2.players.add(player)
        self.table2.save()

    def test_seechips_good(self):
        self.assertEqual(self.ui.check_command("player1:seechips table1"), "Player player1 has 75 chips")
        self.assertEqual(self.ui.check_command("player2:seechips table1"), "Player player2 has 100 chips")
        self.assertEqual(self.ui.check_command("player3:seechips table2"), "Player player3 has 100 chips")
        self.assertEqual(self.ui.check_command("player4:seechips table2"), "Player player4 has 5 chips")

    def test_nonexistent_player_seechips(self):
        self.assertEqual(self.ui.check_command("player7: seechips table1"), "can't see chips because player does not exist")
        self.assertEqual(self.ui.check_command("babysandwich:seechips table2"), "can't see chips because player does not exist")

    def test_seechips_not_enough_args(self):
        self.assertEqual(self.ui.check_command("player3:seechips"), "command must only include 'seechips' <name of table>")
        self.assertEqual(self.ui.check_command("player2:seechips"), "command must only include 'seechips' <name of table>")

    def test_seechips_too_many_args(self):
        self.assertEqual(self.ui.check_command("player3:seechips table2 right now"), "command must only include 'seechips' <name of table>")
        self.assertEqual(self.ui.check_command("player3:seechips inside table1"), "command must only include 'seechips' <name of table>")
        self.assertEqual(self.ui.check_command("player2:seechips table2 5"), "command must only include 'seechips' <name of table>")

    def test_seechips_written_incorrect(self):
        self.assertEqual(self.ui.check_command("player4:seechipstable2 "), "seechips command written incorrectly")
        self.assertEqual(self.ui.check_command("player2:sseechips table1"), "seechips command written incorrectly")

    def test_seechips_nonexistent_table(self):
        self.assertEqual(self.ui.check_command("player1:seechips table3"), "Error: table3 does not exist")
        self.assertEqual(self.ui.check_command("player2:seechips tablee"), "Error: tablee does not exist")
        self.assertEqual(self.ui.check_command("player4:seechips bigtable"), "Error: bigtable does not exist")

    def test_seechips_player_not_in_table(self):
        self.assertEqual(self.ui.check_command("player1:seechips table2"), "Player player1 is not a part of table table2")
        self.assertEqual(self.ui.check_command("player4:seechips table1"), "Player player4 is not a part of table table1")
