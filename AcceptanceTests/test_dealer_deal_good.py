from django.test import TestCase
from project8.models import PlayerModel, DealerModel, AdminModel, CardModel,DeckModel, PokerTableModel
from project8.Classes.Command import Command


class DealerTestDealSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)
        cards = []
        for s in range(1, 5):
            for r in range(1, 14):
                newcard = CardModel.objects.create(suit=s, rank=r)
                cards.append(newcard)
        newdeck = DeckModel.objects.create(name="deck1")
        for card in cards:
            newdeck.cards.add(card)
        newdeck.save()
        maindealer = DealerModel.objects.create(username="MegaSandwich", isonline=True, deck=newdeck)
        self.player1 = PlayerModel.objects.create(username="player1", isonline=True)
        self.player2 = PlayerModel.objects.create(username="player2", isonline=True)
        PokerTableModel.objects.create(name="table1", dealer=maindealer)


    def test_draw_2_players(self):
        table1 = PokerTableModel.objects.get(name__exact="table1")
        table1.players.add(self.player1)
        table1.players.add(self.player2)
        table1.save()
        self.assertEqual(self.ui.check_command("MegaSandwich:deal table1"), "MegaSandwich successfully dealed cards")







