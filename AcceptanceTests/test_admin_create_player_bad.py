from django.test import TestCase
from project8.models import AdminModel, PlayerModel
from project8.Classes.Command import Command

# who: as an admin
# what: I can call command to create player accounts (username only, no passwords)
# why: so that I can see the players

#    Everyone will see list of active players.
#    When admin calls createplayer <playername>" admin can successfully add players online.
#    Everyone will  see online player lists change when they add players (after calling "createplayer <playername>")

# when creating a player with the same username -> "Error can not create <playername> because <playername> already exists")
# typos -> please type in correct command
# no input for playername -> please type in 1 player name
# multiple inputs for playername -> please type in 1 player name

class AdminTestCreateSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)
        PlayerModel.objects.create(username="Player1", isonline=True)
        PlayerModel.objects.create(username="Player2", isonline=False)

    def test_create_player_player_exists(self):
        online_player_list = PlayerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_player_list), 1)
        self.assertEqual(self.ui.check_command("AdminSandwich: createplayer Player1"),
                         "Cannot create Player1 because Player1 already exists")
                        # "\nTables: 'None' \nSeats at table: 'None'")
        self.assertEqual(len(online_player_list), 1)

    def test_create_player_typo(self):
        online_player_list = PlayerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_player_list), 1)
        self.assertEqual(self.ui.check_command("AdminSandwich: createplayers Player2"),
                         "please type in correct command")
                        # "\nTables: 'None' \nSeats at table: 'None'")
        self.assertEqual(self.ui.check_command("AdminSandwich: create player Player2"),
                         "please type in correct command")
                        # "\nTables: 'None' \nSeats at table: 'None'")
        online_player_list = PlayerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_player_list), 1)

    def test_create_player_no_name(self):
        online_player_list = PlayerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_player_list), 1)
        self.assertEqual(self.ui.check_command("AdminSandwich: createplayer "),
                         "Please type in 1 player name")
        online_player_list = PlayerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_player_list), 1)

    def test_create_player_multiple_names(self):
        online_player_list = PlayerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_player_list), 1)
        self.assertEqual(self.ui.check_command("AdminSandwich: createplayer Player1 Player2"),
                         "Please type in 1 player name")
        online_player_list = PlayerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_player_list), 1)


