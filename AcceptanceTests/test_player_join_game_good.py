from django.test import TestCase
from project8.models import AdminModel, PlayerModel, PokerTableModel, DealerModel
from project8.Classes.Command import Command

# As a player I am able to join a table so I can be a part of their game

# AC:
# a. When a player calls join <tablename> they see they have joined that table
# player can join multiple tables and can see they are in those tables
# up to 5 players can join table
# when they join the table, they will see the # of seats at the table go up (webpage)
# b. When an offline player calls join <tablename> they are unable to join
# c. When a player calls join <tablename> on a nonexisting player they are unable to join and see a message that the table doesn't exist

class PlayerTestJoinSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)

        #mutliple tables
        dealer = DealerModel.objects.create(username="MasterSandwich", isonline=True)
        PokerTableModel.objects.create(name="table1", dealer=dealer)
        megadealer = DealerModel.objects.create(username="MegaSandwich", isonline=True)
        PokerTableModel.objects.create(name="table2", dealer=megadealer)

        #players
        PlayerModel.objects.create(username="player1", isonline=True)
        PlayerModel.objects.create(username="player2", isonline=True)
        PlayerModel.objects.create(username="player3", isonline=True)
        PlayerModel.objects.create(username="player4", isonline=True)
        PlayerModel.objects.create(username="player5", isonline=True)
        PlayerModel.objects.create(username="player6", isonline=True)
        PlayerModel.objects.create(username="player7", isonline=True)

    def test_player_join_table(self):
        self.assertEqual(self.ui.check_command("player1:join table1"), "player1 successfully joined table1")

    def test_two_players_join_table(self):
        self.assertEqual(self.ui.check_command("player1:join table1"), "player1 successfully joined table1")
        self.assertEqual(self.ui.check_command("player2:join table1"), "player2 successfully joined table1")

    def test_three_players_join_table(self):
        self.assertEqual(self.ui.check_command("player1:join table1"), "player1 successfully joined table1")
        self.assertEqual(self.ui.check_command("player2:join table1"), "player2 successfully joined table1")
        self.assertEqual(self.ui.check_command("player3:join table1"), "player3 successfully joined table1")

    def test_four_players_join_table(self):
        self.assertEqual(self.ui.check_command("player1:join table1"), "player1 successfully joined table1")
        self.assertEqual(self.ui.check_command("player2:join table1"), "player2 successfully joined table1")
        self.assertEqual(self.ui.check_command("player3:join table1"), "player3 successfully joined table1")
        self.assertEqual(self.ui.check_command("player4:join table1"), "player4 successfully joined table1")

    def test_five_players_join_table(self):
        self.assertEqual(self.ui.check_command("player1:join table1"), "player1 successfully joined table1")
        self.assertEqual(self.ui.check_command("player2:join table1"), "player2 successfully joined table1")
        self.assertEqual(self.ui.check_command("player3:join table1"), "player3 successfully joined table1")
        self.assertEqual(self.ui.check_command("player4:join table1"), "player4 successfully joined table1")
        self.assertEqual(self.ui.check_command("player5:join table1"), "player5 successfully joined table1")

    def test_player_join_different_tables(self):
        self.assertEqual(self.ui.check_command("player1:join table1"), "player1 successfully joined table1")
        self.assertEqual(self.ui.check_command("player1:join table2"), "player1 successfully joined table2")

    def test_multiple_players_join_different_tables(self):
        self.assertEqual(self.ui.check_command("player1:join table1"), "player1 successfully joined table1")
        self.assertEqual(self.ui.check_command("player1:join table2"), "player1 successfully joined table2")

        self.assertEqual(self.ui.check_command("player2:join table1"), "player2 successfully joined table1")
        self.assertEqual(self.ui.check_command("player2:join table2"), "player2 successfully joined table2")

        self.assertEqual(self.ui.check_command("player5:join table2"), "player5 successfully joined table2")
        self.assertEqual(self.ui.check_command("player6:join table2"), "player6 successfully joined table2")
        self.assertEqual(self.ui.check_command("player7:join table2"), "player7 successfully joined table2")