from django.test import TestCase
from project8.models import DealerModel
from project8.models import PlayerModel
from project8.models import PokerTableModel
from project8.Classes.Command import Command
# •	As a player I can fold, so that I can lay down my cards and stop playing for that hand
# o	When the player calls “fold” table name, the section inside the table that specifies whether a player folded will say “True”
# o	If the player is not a part of that table, an error message will appear saying they are not a part of that table
# o	If the table does not exist, an error message will appear specifying that


class TestPlayerFold(TestCase):

    def setUp(self):
        self.ui = Command()

        self.player1 = PlayerModel.objects.create(username="player1", isonline=True, chips=75)
        self.player2 = PlayerModel.objects.create(username="player2", isonline=True, chips=100)
        self.player3 = PlayerModel.objects.create(username="player3", isonline=True, chips=100)
        self.player4 = PlayerModel.objects.create(username="player4", isonline=True, chips=5)
        self.player5 = PlayerModel.objects.create(username="player5", isonline=False)

        self.dealer = DealerModel.objects.create(username="dealer1", isonline=True)
        self.table1 = PokerTableModel.objects.create(name="table1", dealer=self.dealer)
        self.table2 = PokerTableModel.objects.create(name="table2", dealer=self.dealer)

        list_1 = [self.player1, self.player2]
        list_2 = [self.player3, self.player4]

        for player in list_1:
            self.table1.players.add(player)
        self.table1.save()
        for player in list_2:
            self.table2.players.add(player)
        self.table2.save()

    def test_fold_good(self):
        self.assertEqual(self.ui.check_command("player1:fold table1"), "player1 folded")

    def test_nonexistent_player_fold(self):
        self.assertEqual(self.ui.check_command("player7:fold table1"), "can't fold because player does not exist")

    def test_fold_not_enough_args(self):
        self.assertEqual(self.ui.check_command("player3:fold"), "command must only include 'fold' <name of table>")

    def test_fold_written_incorrect(self):
        self.assertEqual(self.ui.check_command("player4:folds table1 "), "fold command written incorrectly")

    def test_fold_nonexistent_table(self):
        self.assertEqual(self.ui.check_command("player1:fold table3"), "Error: table3 does not exist")

    def test_fold_player_not_in_table(self):
        self.assertEqual(self.ui.check_command("player1:fold table2"), "Player player1 is not a part of table table2")