from django.test import TestCase
from project8.models import AdminModel
from project8.Classes.Command import Command

# as an admin, I can login in an admin and check I am the only admin of the game
# so I and all players are aware I am admin
#
# a. Offline Admin can go online by typing admin username in username region,
# and login in command region. There will be a response that shows the admin has logged in,
# along with the number of tables & seats at each table
# b. Admins receives an error message when other Admins try to join the game.
# The response says that “Admin x is already admin, admin Y could not login”
# c. If someone tries to log in to admin with incorrect username,
# there will be a response saying "admin y" cannot login along with
# the number of tables & seats at each table
# d. If admin provides incorrect "login" command such as including extra letters
# (not including leading & trailing whitespace),
# there will be a response saying "login prompt not provided"


class AdminTestSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)
        AdminModel.objects.create(username="AdminSandwich2", isonline=False)
        AdminModel.objects.create(username="FakeAdmin", isonline=False)


    def test_admin_login(self):
        # an offline dealer cannot login and join game
        AdminModel.objects.filter(username__exact='AdminSandwich').update(isonline=False)
        self.mainadmin = AdminModel.objects.get(username__exact='AdminSandwich2')
        # dealer is offline
        self.assertEqual(self.mainadmin.isonline, False)
        self.assertEqual(self.ui.check_command("AdminSandwich2: login"),
                         "Admin \"AdminSandwich2\" logged in successfully")
        # check that dealer is now online
        self.mainadmin = AdminModel.objects.get(username__exact='AdminSandwich2')
        self.assertEqual(self.mainadmin.isonline, True)

    def test_admin_login_command_extra_spaces(self):
        AdminModel.objects.filter(username__exact='AdminSandwich').update(isonline=False)
        self.mainadmin = AdminModel.objects.get(username__exact='AdminSandwich')
        self.assertEqual(self.mainadmin.isonline, False)
        self.assertEqual(self.ui.check_command("AdminSandwich: login "),  # extra spaces
                         "Admin \"AdminSandwich\" logged in successfully")
        self.mainadmin = AdminModel.objects.get(username__exact='AdminSandwich')
        self.assertEqual(self.mainadmin.isonline, True)

