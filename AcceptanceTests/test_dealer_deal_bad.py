from django.test import TestCase
from project8.models import PlayerModel, DealerModel, AdminModel, CardModel,DeckModel, PokerTableModel
from project8.Classes.Command import Command


class DealerTestDealSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)
        cards = []
        for s in range(1, 5):
            for r in range(1, 14):
                newcard = CardModel.objects.create(suit=s, rank=r)
                cards.append(newcard)
        newdeck = DeckModel.objects.create(name="deck1")
        for card in cards:
            newdeck.cards.add(card)
        newdeck.save()
        maindealer = DealerModel.objects.create(username="MegaSandwich", isonline=True, deck=newdeck)
        self.player1 = PlayerModel.objects.create(username="player1", isonline=True)
        self.player2 = PlayerModel.objects.create(username="player2", isonline=True)
        PokerTableModel.objects.create(name="table1", dealer=maindealer)


    def test_draw_one_player(self):
        table1 = PokerTableModel.objects.get(name__exact="table1")
        table1.players.add(self.player1)
        table1.save()
        self.assertEqual(self.ui.check_command("MegaSandwich:deal table1"), "not enough players in table to start game")

    def test_draw_no_table(self):
        self.assertEqual(self.ui.check_command("MegaSandwich:deal"), "command must only include 'deal' <name of table>")

    def test_draw_no_players(self):
        self.assertEqual(self.ui.check_command("MegaSandwich:deal table1"), "not enough players in table to start game")

    def test_draw_nonexistent_dealer(self):
        self.assertEqual(self.ui.check_command("Sandwich:deal table1"), "can't deal because dealer does not exist")

    def test_draw_typo(self):
        self.assertEqual(self.ui.check_command("MegaSandwich:dealll table1"), "deal command written incorrectly")
        self.assertEqual(self.ui.check_command("MegaSandwich:deal! table1"), "deal command written incorrectly")

    def test_draw_nonexistent_table(self):
        self.assertEqual(self.ui.check_command("MegaSandwich:deal table2"), "could not deal cards: table does not exist")

