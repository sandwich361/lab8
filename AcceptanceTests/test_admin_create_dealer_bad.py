from django.test import TestCase
from project8.models import AdminModel, DealerModel
from project8.Classes.Command import Command

# who: as an admin
# what: I can call command to create dealer accounts (username only, no passwords)
# why: so that I can see the dealers

#    Everyone will see list of active dealers.
#    When admin calls createdealer <dealername>" admin can successfully add dealers online.
#    Everyone will  see online dealer lists change when they add dealers (after calling "createdealer <dealername>")

# when creating a dealer with the same username -> "Error can not create <dealername> because <dealername> already exists")
# typos -> please type in correct command
# no input for dealername -> please type in 1 dealer name
# multiple inputs for dealername -> please type in 1 dealer name

class AdminTestCreateSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)
        DealerModel.objects.create(username="Dealer1", isonline=True)
        DealerModel.objects.create(username="Dealer2", isonline=False)

    def test_create_dealer_dealer_exists(self):
        online_dealer_list = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealer_list), 1)
        self.assertEqual(self.ui.check_command("AdminSandwich: createdealer Dealer1"),
                         "Cannot create Dealer1 because Dealer1 already exists")

        self.assertEqual(len(online_dealer_list), 1)

    def test_create_dealer_typo(self):
        online_dealer_list = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealer_list), 1)
        self.assertEqual(self.ui.check_command("AdminSandwich: createdealers Dealer2"),
                         "please type in correct command")

        self.assertEqual(self.ui.check_command("AdminSandwich: create dealer Dealer1"),
                         "please type in correct command")
                        # "\nTables: 'None' \nSeats at table: 'None'")
        self.assertEqual(self.ui.check_command("AdminSandwich: create a dealer Dealer3"),
                         "please type in correct command")
        online_dealer_list = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealer_list), 1)

    def test_create_dealer_no_name(self):
        online_dealer_list = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealer_list), 1)
        self.assertEqual(self.ui.check_command("AdminSandwich: createdealer "),
                         "Please type in 1 dealer name")
        online_dealer_list = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealer_list), 1)

    def test_create_dealer_multiple_names(self):
        online_dealer_list = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealer_list), 1)
        self.assertEqual(self.ui.check_command("AdminSandwich: createdealer Dealer1 Dealer3"),
                         "Please type in 1 dealer name")
        online_dealer_list = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealer_list), 1)
