from django.test import TestCase
from project8.models import DealerModel
from project8.models import PlayerModel
from project8.models import PokerTableModel
from project8.Classes.Command import Command

# As a player I can use a “call” command so that I can bet an amount of chips matching the table’s current bet.
# o	When a player calls “call” <tablename>, their chips are decreased by the number of table’s current bet.
# o	The table’s pot will increase by table’s current bet.
# o	If the player is not a part of that table, an error message will appear saying they are not a part of that table
# o	If the table does not exist, an error message will appear specifying that
# o	If the players chips are less than the current bet, the player cannot “call” and an error message will appear saying they must go allin or fold.

class TestPlayerCall(TestCase):

    def setUp(self):
        self.ui = Command()

        self.player1 = PlayerModel.objects.create(username="player1", isonline=True, chips=75)
        self.player2 = PlayerModel.objects.create(username="player2", isonline=True, chips=50)
        self.player3 = PlayerModel.objects.create(username="player3", isonline=True, chips=100)
        self.player4 = PlayerModel.objects.create(username="player4", isonline=True, chips=15)
        self.player5 = PlayerModel.objects.create(username="player5", isonline=False, chips=0)

        self.dealer = DealerModel.objects.create(username="dealer1", isonline=True)
        self.table1 = PokerTableModel.objects.create(name="table1", dealer=self.dealer)
        self.table2 = PokerTableModel.objects.create(name="table2", dealer=self.dealer)

        list_1 = [self.player1, self.player2]
        list_2 = [self.player3, self.player4]

        for player in list_1:
            self.table1.players.add(player)
        self.table1.save()
        for player in list_2:
            self.table2.players.add(player)
        self.table2.save()

    def test_call_good(self):
        self.table1.currentBet = 45
        self.table1.save()
        self.table2.currentBet = 5
        self.table2.save()
        self.assertEqual(self.ui.check_command("player1:call table1"), "player1 called")
        self.assertEqual(self.ui.check_command("player4:call table2"), "player4 called")

    def test_call_current_bet_zero(self):
        self.table1.currentBet = 0
        self.table1.save()
        self.table2.currentBet = 0
        self.table2.save()
        self.assertEqual(self.ui.check_command("player2:call table1"), "Error: current bet is 0 can not call on a 0 bet")
        self.assertEqual(self.ui.check_command("player3:call table2"), "Error: current bet is 0 can not call on a 0 bet")

    def test_call_not_enough_chips(self):
        self.table1.currentBet = 60
        self.table1.save()
        self.table2.currentBet = 30
        self.table2.save()
        #TODO check the bets?
        print("player 2:"+str(self.player2.chips))
        print("player 3:" + str(self.player3.chips))
        print("current bet:" + str(self.table2.currentBet))

        self.assertEqual(self.ui.check_command("player2:call table1"), "Error: player must go allin or fold")
        self.assertEqual(self.ui.check_command("player3:call table2"), "Error: player must go allin or fold")

    def test_nonexistent_player_call(self):
        self.assertEqual(self.ui.check_command("player7:call table1"), "can't call because player does not exist")
        self.assertEqual(self.ui.check_command("babysandwich:call table2"), "can't call because player does not exist")

    def test_call_not_enough_args(self):
        self.assertEqual(self.ui.check_command("player3:call"), "command must only include 'call' <name of table>")
        self.assertEqual(self.ui.check_command("player2:call"), "command must only include 'call' <name of table>")

    def test_call_too_many_args(self):
        self.assertEqual(self.ui.check_command("player3:call table2 right now"), "command must only include 'call' <name of table>")
        self.assertEqual(self.ui.check_command("player3:call inside table2"), "command must only include 'call' <name of table>")
        self.assertEqual(self.ui.check_command("player2:call table2 5"), "command must only include 'call' <name of table>")

    def test_call_written_incorrect(self):
        self.assertEqual(self.ui.check_command("player4:calls table1 "), "call command written incorrectly")
        self.assertEqual(self.ui.check_command("player2:calltable1"), "call command written incorrectly")

    def test_call_nonexistent_table(self):
        self.assertEqual(self.ui.check_command("player1:call table3"), "Error: table3 does not exist")
        self.assertEqual(self.ui.check_command("player2:call tablee"), "Error: tablee does not exist")
        self.assertEqual(self.ui.check_command("player4:call bigtable"), "Error: bigtable does not exist")

    def test_call_player_not_in_table(self):
        self.assertEqual(self.ui.check_command("player1:call table2"), "Player player1 is not a part of table table2")
        self.assertEqual(self.ui.check_command("player4:call table1"), "Player player4 is not a part of table table1")