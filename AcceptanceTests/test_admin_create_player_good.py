from django.test import TestCase
from project8.models import AdminModel, PlayerModel
from project8.Classes.Command import Command

# who: as an admin
# what: I can call command to create player accounts (username only, no passwords)
# why: so that I can see the players

#    Everyone will see list of active players.
#    When admin calls createplayer <playername>" admin can successfully add players online.
#    Everyone will  see online player lists change when they add players (after calling "createplayer <playername>")

#offline player will be made online
#players that don't exist will be created & made online

class AdminTestCreateSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)
        PlayerModel.objects.create(username="Player1", isonline=False)

    def test_admin_create_nonexistent_player(self):
        online_player_list = PlayerModel.objects.filter(isonline=True)
        print(online_player_list)
        self.assertEqual(len(online_player_list), 0)
        self.assertEqual(self.ui.check_command("AdminSandwich: createplayer MegaSandwich"),
                             "Created player: MegaSandwich")
        online_player = PlayerModel.objects.get(isonline=True)
        self.assertEqual(online_player.username, "MegaSandwich")

    #player is already in database but not online
    def test_admin_create_offline_player_goes_online(self):
        online_player_list = PlayerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_player_list), 0)
        self.assertNotIn("Player1", online_player_list)
        print(online_player_list)
        self.assertEqual(self.ui.check_command("AdminSandwich: createplayer Player1"),
                         "Created player: Player1")
        online_player = PlayerModel.objects.get(isonline=True)
        self.assertEqual(online_player.username, "Player1")



    def test_admin_create_multiple_players(self):
        online_player_list = PlayerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_player_list), 0)
        self.assertEqual(self.ui.check_command("AdminSandwich: createplayer Player1"),
                         "Created player: Player1")
        self.assertEqual(self.ui.check_command("AdminSandwich: createplayer Player2"),
                         "Created player: Player2")
        online_player_list = PlayerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_player_list), 2)


