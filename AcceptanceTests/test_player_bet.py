from django.test import TestCase
from project8.models import DealerModel
from project8.models import PlayerModel
from project8.models import PokerTableModel
from project8.Classes.Command import Command
# •	As a player I can call a ‘bet’ command so that some of my chips can got in the table pot
# o	When a player calls “bet” <amount> <tablename>, that amount of chips will be added to the table pot and taken away from the player’s chip count.
# o	The table’s current bet will change to the bet amount specified
# o	If the player is not a part of that table, an error message will appear saying they are not a part of that table
# o	If the table does not exist, an error message will appear specifying that
# o	If the amount entered is more than the amount of chips the player has, an error message will appear specifying that the player doesn’t have enough chips
# o	If the bet amount is not an integer, an error message will appear and the player will not be able to bet
# o	If the bet amount is negative, an error message will appear, and the player will not be able to bet


class TestPlayerBet(TestCase):

    def setUp(self):
        self.ui = Command()

        self.player1 = PlayerModel.objects.create(username="player1", isonline=True, chips=75)
        self.player2 = PlayerModel.objects.create(username="player2", isonline=True, chips=100)
        self.player3 = PlayerModel.objects.create(username="player3", isonline=True, chips=100)
        self.player4 = PlayerModel.objects.create(username="player4", isonline=True, chips=5)
        self.player5 = PlayerModel.objects.create(username="player5", isonline=False)

        self.dealer = DealerModel.objects.create(username="dealer1", isonline=True)
        self.table1 = PokerTableModel.objects.create(name="table1", dealer=self.dealer)
        self.table2 = PokerTableModel.objects.create(name="table2", dealer=self.dealer)

        list_1 = [self.player1, self.player2]
        list_2 = [self.player3, self.player4]

        for player in list_1:
            self.table1.players.add(player)
        self.table1.save()
        for player in list_2:
            self.table2.players.add(player)
        self.table2.save()

    def test_bet_good(self):
        self.assertEqual(self.ui.check_command("player1:bet 5 table1"), "player1 successfully bet 5 chips")
        self.assertEqual(self.ui.check_command("player2:bet 5 table1"), "player2 successfully bet 5 chips")
        self.assertEqual(self.ui.check_command("player4:bet 5 table2"), "player4 successfully bet 5 chips")

    def test_nonexistent_player_bet(self):
        self.assertEqual(self.ui.check_command("player7:bet 20 table1"), "can't bet because player does not exist")
        self.assertEqual(self.ui.check_command("babysandwich:bet 60 table2"), "can't bet because player does not exist")

    def test_nonInteger_bet(self):
        self.assertEqual(self.ui.check_command("player1:bet abc table1"), "bet amount not an integer")
        self.assertEqual(self.ui.check_command("player1:bet 7.5 table1"), "bet amount not an integer")
        self.assertEqual(self.ui.check_command("player2:bet {1} table1"), "bet amount not an integer")

    def test_bet_not_enough_args(self):
        self.assertEqual(self.ui.check_command("player3:bet 5"), "command must only include 'bet' <amount> <name of table>")
        self.assertEqual(self.ui.check_command("player3:bet"), "command must only include 'bet' <amount> <name of table>")
        self.assertEqual(self.ui.check_command("player3:bet table2"), "command must only include 'bet' <amount> <name of table>")

    def test_bet_too_many_args(self):
        self.assertEqual(self.ui.check_command("player3:bet 5 table2 right now"), "command must only include 'bet' <amount> <name of table>")
        self.assertEqual(self.ui.check_command("player3:bet 71 chips allin"), "command must only include 'bet' <amount> <name of table>")
        self.assertEqual(self.ui.check_command("player3:bet 67 109 23 table2"), "command must only include 'bet' <amount> <name of table>")

    def test_bet_written_incorrect(self):
        self.assertEqual(self.ui.check_command("player4: bet67 "), "bet command written incorrectly")
        self.assertEqual(self.ui.check_command("player2:bett 67 table1"), "bet command written incorrectly")

    def test_not_enough_chips(self):
        self.assertEqual(self.ui.check_command("player4:bet 10 table2"), "Error: not enough chips")
        self.assertEqual(self.ui.check_command("player1:bet 80 table1"), "Error: not enough chips")
        self.assertEqual(self.ui.check_command("player2:bet 105 table1"), "Error: not enough chips")

    def test_negative_bet(self):
        self.assertEqual(self.ui.check_command("player1:bet -15 table1"), "Error: can't bet negative amount")
        self.assertEqual(self.ui.check_command("player3:bet -50 table2"), "Error: can't bet negative amount")

    def test_nonexistent_table(self):
        self.assertEqual(self.ui.check_command("player1:bet 5 table3"), "Error: table3 does not exist")
        self.assertEqual(self.ui.check_command("player2:bet 5 tablee"), "Error: tablee does not exist")
        self.assertEqual(self.ui.check_command("player4:bet 5 bigtable"), "Error: bigtable does not exist")

    def test_player_not_in_table(self):
        self.assertEqual(self.ui.check_command("player1:bet 5 table2"), "Player player1 is not a part of table table2")
        self.assertEqual(self.ui.check_command("player4:bet 5 table1"), "Player player4 is not a part of table table1")

