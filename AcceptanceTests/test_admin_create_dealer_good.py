from django.test import TestCase
from project8.models import AdminModel, DealerModel
from project8.Classes.Command import Command

# who: as an admin
# what: I can call command to create player accounts (username only, no passwords)
# why: so that I can see the players

#    Everyone will see list of online dealers.
#    When admin calls createdealer <dealername>" admin can successfully add dealers online.
#    Everyone will  see online dealer lists change when they add dealers (after calling createdealer <dealername>)

#offline dealer will be made online
#players that don't exist will be created & made online

class AdminTestCreateSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)
        DealerModel.objects.create(username="MegaSandwich", isonline=False)

    def test_admin_create_nonexistent_dealer(self):
        online_dealer_list = DealerModel.objects.filter(isonline=True)
        print(online_dealer_list)
        self.assertEqual(len(online_dealer_list), 0)
        self.assertEqual(self.ui.check_command("AdminSandwich: createdealer Dealer1"),
                             "Created dealer: Dealer1")
        online_dealer_list = DealerModel.objects.filter(isonline=True)
        online_dealer = DealerModel.objects.get(isonline=True)
        self.assertEqual(online_dealer.username, "Dealer1")
        self.assertEqual(len(online_dealer_list), 1)

    #player is already in database but not online
    def test_admin_create_offline_dealer_goes_online(self):
        online_dealer_list = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealer_list), 0)
        self.assertNotIn("MegaSandwich", online_dealer_list)
        print(online_dealer_list)
        self.assertEqual(self.ui.check_command("AdminSandwich: createdealer MegaSandwich"),
                         "Created dealer: MegaSandwich")
        online_dealer_list = DealerModel.objects.filter(isonline=True)
        online_dealer = DealerModel.objects.get(isonline=True)
        self.assertEqual(online_dealer.username, "MegaSandwich")
        self.assertEqual(len(online_dealer_list), 1)



    def test_admin_create_multiple_dealers(self):
        online_dealer_list = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealer_list), 0)
        self.assertEqual(self.ui.check_command("AdminSandwich: createdealer Dealer1"),
                         "Created dealer: Dealer1")
        self.assertEqual(self.ui.check_command("AdminSandwich: createdealer Dealer2"),
                         "Created dealer: Dealer2")
        online_dealer_list = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealer_list), 2)


