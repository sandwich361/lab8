from django.test import TestCase
from project8.models import PlayerModel
from project8.Classes.Command import Command


# who: As a player,
# what: I can call commands to play in a game with other players
# why: so that I 'can' play poker
class TestCanPlay(TestCase):

    # creating a player object for the tests
    def setUp(self):
        self.ui = Command()
        self.player1 = PlayerModel.objects.create(username="player1", isonline=True, chips=100)
        self.player1a = PlayerModel.objects.create(username="player1", isonline=True, chips=100)
        self.player2 = PlayerModel.objects.create(username="player2", isonline=True, chips=100)
        self.player3 = PlayerModel.objects.create(username="player3", isonline=False, chips=100)

    # calls join_game() on the player. This method will handle exceptions and return true
    # if the player successfully joins the game.
    def test_can_play(self):
        self.assertEqual(self.ui.check_command("player2: join"), "player2 joined the game")
        self.assertEqual(self.ui.check_command("player1: join"), "player2 joined the game")
        self.assertEqual(self.ui.check_command("player1a: join"), "player1a cannot join the game, already player 1")
        self.assertEqual(self.ui.check_command("player3: join"), "player3 cannot join the game, not online")
        self.assertEqual(self.ui.check_command("player2: joiinnn"), 'please type in correct command')
