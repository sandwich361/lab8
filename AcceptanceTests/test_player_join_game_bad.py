from django.test import TestCase
from project8.models import AdminModel, PlayerModel, PokerTableModel, DealerModel
from project8.Classes.Command import Command

# As a player I am able to join a table so I can be a part of their game

# AC:
# a. When a player calls join <tablename> they see they have joined that table
# b. When an offline player calls join <tablename> they are unable to join
# c. When a player calls join <tablename> on a nonexisting player they are unable to join and see a message that the table doesn't exist
# already have 5 players -> tablename cannot add more players
# player already in table -> playername is in tablename
# offline player -> tablename cannot add playername
# table doesnt exist -> cannot access tablename
# typos -> please type in correct command
# no table provided -> please type in 1 tablename
# multiple tables -> please type in 1 tablename

class PlayerTestJoinSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)

        #mutliple tables
        dealer = DealerModel.objects.create(username="MasterSandwich", isonline=True)
        PokerTableModel.objects.create(name="table1", dealer=dealer)
        megadealer = DealerModel.objects.create(username="MegaSandwich", isonline=True)
        PokerTableModel.objects.create(name="table2", dealer=megadealer)

        #players
        PlayerModel.objects.create(username="player1", isonline=True)
        PlayerModel.objects.create(username="player2", isonline=True)
        PlayerModel.objects.create(username="player3", isonline=True)
        PlayerModel.objects.create(username="player4", isonline=True)
        PlayerModel.objects.create(username="player5", isonline=True)
        PlayerModel.objects.create(username="player6", isonline=True)
        PlayerModel.objects.create(username="player7", isonline=True)
        PlayerModel.objects.create(username="offlineplayer", isonline=False)


    def test_join_six_players(self):
        self.assertEqual(self.ui.check_command("player1:join table1"), "player1 successfully joined table1")
        self.assertEqual(self.ui.check_command("player2:join table1"), "player2 successfully joined table1")
        self.assertEqual(self.ui.check_command("player3:join table1"), "player3 successfully joined table1")
        self.assertEqual(self.ui.check_command("player4:join table1"), "player4 successfully joined table1")
        self.assertEqual(self.ui.check_command("player5:join table1"), "player5 successfully joined table1")

        self.assertEqual(self.ui.check_command("player6:join table1"), "table1 cannot add more players")

    def test_player_already_in_table(self):
        self.assertEqual(self.ui.check_command("player1:join table1"), "player1 successfully joined table1")

        #joining again
        self.assertEqual(self.ui.check_command("player1:join table1"), "player1 is already in table1")

    def test_offline_player(self):
        self.assertEqual(self.ui.check_command("nonexistentplayer:join table1"), "cannot join because player does not exist")
        self.assertEqual(self.ui.check_command("offlineplayer:join table1"), "cannot join because player does not exist")


    def test_join_nonexistent_table(self):
        self.assertEqual(self.ui.check_command("player1:join table3"), "cannot join because table3 does not exist")

    def test_join_typos(self):
        self.assertEqual(self.ui.check_command("player1: joinnn table1"), "join command written incorrectly")
        self.assertEqual(self.ui.check_command("player1: join!!! table2"), "join command written incorrectly")

    def test_join_no_tablename(self):
        self.assertEqual(self.ui.check_command("player1: join"), "please type in 1 tablename")

    def test_join_multiple_tablenames(self):
        self.assertEqual(self.ui.check_command("player1: join table1 table2"), "please type in 1 tablename")

