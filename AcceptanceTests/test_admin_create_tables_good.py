from django.test import TestCase
from project8.models import AdminModel, PokerTableModel, DealerModel
from project8.Classes.Command import Command

# as an admin, I can create tables and assign them to dealers &
# assign players to tables, so everyone knows who's playing where

# a. When admin calls "createtable <tablename>"
# a table is created and automatically assigned
# to one online dealer (the first one without a table),
# dealer and table are displayed on screen


class AdminTestCreateSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)
        maindealer = DealerModel.objects.create(username="MegaSandwich", isonline=True)


    #best case: dealer is online, no table, assign table to dealer
    def test_create_table_(self):
        self.assertEqual(self.ui.check_command("AdminSandwich: createtable Table1"),
                         "Table1 created & assigned to Dealer \"MegaSandwich\"")
        maindealer = DealerModel.objects.get(username="MegaSandwich")
        table1 = PokerTableModel.objects.get(name__exact="Table1")
        self.assertEqual(table1.dealer, maindealer)

    def test_create_two_tables(self):
        maindealer2 = DealerModel.objects.create(username="MasterSandwich", isonline=True)
        online_dealers = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealers), 2)

        self.assertEqual(self.ui.check_command("AdminSandwich: createtable Table1"),
                         "Table1 created & assigned to Dealer \"MegaSandwich\"")
        active_tables = PokerTableModel.objects.all()


        #create 2nd table
        self.assertEqual(self.ui.check_command("AdminSandwich: createtable Table2"),
                         "Table2 created & assigned to Dealer \"MasterSandwich\"")
        table2 = PokerTableModel.objects.get(name__exact="Table2")
        self.assertEqual(table2.dealer, maindealer2)

    def test_create_three_tables(self):
        maindealer2 = DealerModel.objects.create(username="MasterSandwich", isonline=True)
        maindealer3 = DealerModel.objects.create(username="DealerSandwich", isonline=True)

        online_dealers = DealerModel.objects.filter(isonline=True)
        self.assertEqual(len(online_dealers), 3)

        self.assertEqual(self.ui.check_command("AdminSandwich: createtable Table1"),
                         "Table1 created & assigned to Dealer \"MegaSandwich\"")
        active_tables = PokerTableModel.objects.all()


        # create 2nd table
        self.assertEqual(self.ui.check_command("AdminSandwich: createtable Table2"),
                         "Table2 created & assigned to Dealer \"MasterSandwich\"")
        table2 = PokerTableModel.objects.get(name__exact="Table2")
        self.assertEqual(table2.dealer, maindealer2)

        #create 3rd table
        self.assertEqual(self.ui.check_command("AdminSandwich: createtable Table3"),
                         "Table3 created & assigned to Dealer \"DealerSandwich\"")
        table3 = PokerTableModel.objects.get(name__exact="Table3")
        self.assertEqual(table3.dealer, maindealer3)


