from django.test import TestCase
from project8.models import AdminModel
from project8.Classes.Command import Command

# as an admin, I can login in an admin and check I am the only admin of the game
# so I and all players are aware I am admin
#
# a. Offline Admin can go online by typing admin username in username region,
# and login in command region. There will be a response that shows the admin has logged in,
# along with the number of tables & seats at each table
# b. Admins receives an error message when other Admins try to join the game.
# The response says that “Admin x is already admin, admin Y could not login”
# c. If someone tries to log in to admin with incorrect username,
# there will be a response saying "admin y" cannot login along with
# the number of tables & seats at each table
# d. If admin provides incorrect "login" command such as including extra letters
# (not including leading & trailing whitespace),
# there will be a response saying "login prompt not provided"

class AdminTestSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)
        AdminModel.objects.create(username="FakeAdmin", isonline=False)


    #no multiple admins
    def test_multiple_online_admins_cannot_login(self):
        #  Dealer receives an error message when other dealers try to join the game.
        self.admins = AdminModel.objects.filter(isonline=True)
        print(self.admins)  # [<AdminModel: AdminSandwich>]
        # one admin already logged in
        firstadmin_username = self.admins[0].username
        self.admins.update(isonline=False)
        self.assertEqual(self.ui.check_command(firstadmin_username + ": login"),
                         "Admin \"" + firstadmin_username + "\" logged in successfully")
        # no other admin can log in
        self.assertEqual(self.ui.check_command("FakeAdmin: login"),
                         "Error: " + firstadmin_username +" is already the admin")
    #typos
    def test_admin_incorrect_login_command(self):
        # dealer types in wrong command prompt
        AdminModel.objects.filter(username="AdminSandwich").update(isonline=False)
        self.assertEqual(self.ui.check_command("AdminSandwich: log in"),
                         "please type in correct command")
        self.assertEqual(self.ui.check_command("AdminSandwich: log"),
                         "please type in correct command")
    #empty username
    def test_admin_empty_username(self):
        self.assertEqual(self.ui.check_command(": login"),
                         'Error: Cannot login. No username provided')
    #incorrect usernames
    def test_dealer_incorrect_login_username(self):
        AdminModel.objects.filter(username="AdminSandwich").update(isonline=False)
        self.assertEqual(self.ui.check_command("1: login"), "Error: Admin \"""1\" cannot login")
