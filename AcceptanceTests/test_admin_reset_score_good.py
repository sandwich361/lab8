from django.test import TestCase
from project8.models import AdminModel, PlayerModel, DealerModel
from project8.Classes.Command import Command

# As an admin, I can reset the high score, so players restarts

# a. when admin calls "resetscore <playername>",
# the total score of that player is reset to 0

class AdminTestCreateSuite(TestCase):
    def setUp(self):
        self.ui = Command()
        AdminModel.objects.create(username="AdminSandwich", isonline=True)
        maindealer = DealerModel.objects.create(username="MegaSandwich", isonline=True)
        PlayerModel.objects.create(username="player1", isonline=True)

    def test_reset_score_good(self):
        newplayer = PlayerModel.objects.get(username__exact="player1")
        newplayer.highscore = 15
        newplayer.save()
        self.assertEqual(self.ui.check_command("AdminSandwich: resetscore player1"),
                         "player1 high score reset to 0")
        newplayer = PlayerModel.objects.get(username__exact="player1")
        self.assertEqual(newplayer.highscore, 0)

    def test_reset_score_extra_spaces(self):
        self.assertEqual(self.ui.check_command("AdminSandwich: resetscore  player1"),
                         "player1 high score reset to 0")
        newplayer = PlayerModel.objects.get(username__exact="player1")
        self.assertEqual(newplayer.highscore, 0)

